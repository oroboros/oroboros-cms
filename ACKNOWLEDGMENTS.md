## Acknowledgments ##

Thanks to the following individuals and organizations for their contributions to our efforts, either directly or indirectly.

---
### Organizations ###

The following organizations efforts have proven essential to the development of this project.

#### [![](https://avatars1.githubusercontent.com/u/468401?v=3&s=200) <br>PHP-FIG (Framework Interoperability Group)](http://www.php-fig.org) ####

Thanks for providing a sane set of standards for PHP development that most everyone arguably agrees on. Coming from someone who wrote PHP back in it's early days, it cannot be expressed how much this was needed.

---

### Websites ###

The following websites provide resources that we could not have created this system without.

#### Programming Resources ####

#### [Stack Overflow](http://stackoverflow.org) ####

Anyone who has been programming long enough to have come across any bug or issue they need to look up an answer to understands why this site is awesome.

---

#### [MDN (Mozilla Developer Network)](http://developer.mozilla.org) ####

This is a great resource for *accurate*, and more importantly *human-readable* documentation of a number of programming topics. Documentation is one of the primary areas of neglect in the programming world; we all hate doing it, and we all hate not having it. When it does exist, it is usually either painfully shortsighted or written as a bunch of technical gibberish that only the actual development team understands. Extra thanks to these guys for going the extra mile where it is desperately needed.

---

#### [Github](http://github.org) ####

This is the easiest place to find an open-source solution to your programming woes, and also the best place to share your own solutions. People who participate here frequently tend to be stronger programmers in general, due to the time they spend addressing issues posted by the community on their projects, and are also typically more aware of what other solutions to common issues exist and are stable enough to rely on.

---

#### [Bitbucket](http://bitbucket.org) ####

This is a similar service to Github, but allows for users to host free private repositories as well. Great utility for freelancers and startups who don't want to set aside money in their budget to host repos remotely, can't share them publically, but still need remote version control. This system is currently hosted on Bitbucket.

---

#### Education & Training ####

#### [MIT Open Courseware](http://ocw.mit.edu) ####

If you are serious about improving your knowledge of computer science and already completed college *(or can't afford college, but are dedicated enough to put in the effort on your own)*, this is about the best place you can go to find professional level education material for free. The materials here are actually *better than most of the paid training services* you are likely to run across. There are other schools also participating in the Open Courseware project, but we have found this to be an exceptional set of resources that is generally not available without many tens of thousands of dollars per year in tuition.

---

#### [Khan Academy](http://www.khanacademy.org) ####

It is our belief that a good programmer spends a lot of time learning new ways to code, but a great programmer spends more time learning logic and math skills, in addition to learning how to code. Khan Academy provides very easy to follow screencasts in a number of topics. We found the math and science training to be exceptionally helpful. They do have some computer science material also, but it is much more suited to novice and beginner programmers. Everyone can use a good refresher in math from time to time though, and this is a great place to get it.

---

### Projects ###

The following projects have proven to be essential assets or critical dependencies of this project.

@TODO

### Individuals ###

The following individuals have provided extensive support, knowledge or assistance in the creation of this project.

@TODO

### Team ###

Our team of developers, and any contact info they wish to share globally.

@TODO