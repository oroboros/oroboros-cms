<?php

namespace oroboros;

/*
 * The MIT License
 *
 * Copyright 2016 Brian Dayhoff <brian@mopsyd.me>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * Oroboros Global Accessor
 * ------------------------
 * This class exposes the Oroboros API to other classes, 
 * executing calls to the core system. All other functionality
 *  is deferred to other classes in keeping with the 
 * Single Responsibility Principle.
 */
final class Oroboros implements \oroboros\src\lib\common\interfaces\api\OroborosAPIInterface, \oroboros\src\lib\common\interfaces\api\OroborosExceptionCodeInterface, \oroboros\src\lib\common\interfaces\psr3\LogLevel, \oroboros\src\lib\common\interfaces\psr3\LoggerAwareInterface {

    const LOGGER_DEFAULT = "\\oroboros\\src\\lib\\common\\libraries\\logger\\NullLogger";
    const LOGGER_FILE = "\\oroboros\\src\\lib\\common\\libraries\\logger\\FileLogger";
    const LOGGER_DATABASE = "\\oroboros\\src\\lib\\common\\libraries\\logger\\DatabaseLogger";
    const LOGGER_NULL = "\\oroboros\\src\\lib\\common\\libraries\\logger\\NullLogger";
    const LOGGER_SCREEN = "\\oroboros\\src\\lib\\common\\libraries\\logger\\ScreenLogger";
    const LOGGER_CLI = "\\oroboros\\src\\lib\\common\\libraries\\logger\\CliLogger";
    const LOGGER_CSS = "\\oroboros\\src\\lib\\common\\libraries\\logger\\CssLogger";
    const LOGGER_JS = "\\oroboros\\src\\lib\\common\\libraries\\logger\\JsLogger";
    const LOGGER_AJAX = "\\oroboros\\src\\lib\\common\\libraries\\logger\\AjaxLogger";
    const LOGGER_DEFAULT_FILE = 'errors.log';
    const SETTINGS_DEFAULT = '/config/settings.json';

    private static $_log_modes = array(
        "default" => self::LOGGER_DEFAULT,
        "file" => self::LOGGER_FILE,
        "database" => self::LOGGER_DATABASE,
        "null" => self::LOGGER_NULL,
        "screen" => self::LOGGER_SCREEN,
        "cli" => self::LOGGER_CLI,
        "css" => self::LOGGER_CSS,
        "js" => self::LOGGER_JS,
        "ajax" => self::LOGGER_AJAX,
    );
    private static $_valid_log_levels = [
        self::EMERGENCY,
        self::ALERT,
        self::CRITICAL,
        self::ERROR,
        self::WARNING,
        self::NOTICE,
        self::INFO,
        self::DEBUG,
    ];
    private static $_env;
    private static $_initialized = FALSE;
    private static $_logger;
    private static $_autoloader;
    private static $_settings = array();
    private static $_codex;
    private static $_mode;
    private static $_valid_modes = array();

    public function __construct(array $options = array(), array $flags = array()) {
        if (!self::$_initialized) {
            $this->initialize($options, $flags);
        }
    }

    /**
     * Aliases the static setLoggerObject method, for Psr-3 compliance.
     * This method MAY be used to set the logger, but this class does not 
     * otherwise need to be instantiated.
     * @param \oroboros\src\lib\common\api\psr3\LoggerInterface $logger
     */
    public function setLogger(\Psr\Log\LoggerInterface $logger) {
        self::setLoggerObject($logger);
    }

    /**
     * Proxy for static init
     * @param array $options
     * @param array $flags
     */
    public function initialize(array $options = array(), array $flags = array()) {
        self::init($options, $flags);
    }

    public static function init(array $options = array(), array $flags = array()) {
        if (!empty($options)) {
            //parse custom load options
            $options['core'] = ((isset($options['core'])) ? $options['core'] : self::_getDefaultSettings()['core']);
        } else {
            //bootstrap normally
            $options = self::_getDefaultSettings();
        }
        self::$_settings = $options;
        self::_setLogger();
        self::$_initialized = TRUE;
    }

    /**
     * Returns a system-compliant absolute file path 
     * from a supplied relative path, or alternately 
     * from the location it was called.
     * NOTE: DO NOT USE WITHOUT SUPPLYING A PATH IN CLOSURES. 
     * If used in a closure, pass __DIR__ as the path, 
     * or it will throw an exception.
     * @throws \oroboros\src\lib\common\libraries\exception\OroborosException If called within a closure without supplying a path parameter, which will break execution otherwise.
     * @param string|\Directory|\File $path (optional) The specified relative path. Also supports native PHP file and directory objects. If not supplied, will attempt to return the path to the file where it was called.
     * @return string|boolean Returns [false] if path is not valid. Otherwise returns the fully qualified absolute path.
     */
    public static function filepath($path = NULL) {
        $result = false;
        if ($path) {
            echo 'foooo';
            var_dump(self::OROBOROS_BASEPATH . DIRECTORY_SEPARATOR);
        } else {
            //perform a backtrace to determine where the method was called, find the file from the last valid entry.
        };
        return $result;
    }

    /**
     * Returns a new instance of the logger, 
     * already initialized to the current core settings.
     * You may use this to quickly grab a logger instance from anywhere.
     * Individual instances are prototyped against the CURRENT GLOBAL LOGGER. 
     * This is not a singleton, so you will need to check that you have the 
     * one you need, otherwise use the factory to build one.
     * @param \oroboros\src\lib\common\api\psr3\LoggerInterface $logger
     */
    public static function getLoggerObject() {
        if (!self::$_initialized) {
            self::init();
        }
        return clone(self::$_logger);
    }

    /**
     * <Pre-Initialization Methods>
     * You should probably just call the bootload routine instead of these 
     * unless you have a really good reason for doing so.
     */
    public static function setAutoloader(\oroboros\src\lib\common\libraries\psr4\Autoloader $autoloader) {
        self::$_autoloader = $autoloader;
    }

    /**
     * Sets a logger object. Must implement the provided copy of the psr3 logger interface
     * @param \oroboros\src\lib\common\api\psr3\LoggerInterface $logger
     */
    public static function setLoggerObject(\oroboros\src\lib\common\interfaces\psr3\LoggerInterface $logger) {
        self::$_logger = $logger;
    }

    /**
     * Logs an entry using whatever Psr3 Compliant 
     * logger the system currently has.
     * @return type
     */
    public static function log($level, $message, array $context = array()) {
        if (!self::$_initialized) {
            self::init();
        }
        return self::$_logger->log($level, $message, $context);
    }

    /**
     * <Configuration Deployer>
     * @since 0.0.1a
     * This method fetches parts of the initialization settings. 
     * If the optional key is supplied, it will attempt to return 
     * that section, or false if it does not exist. Otherwise it 
     * returns the entire settings index.
     * @param type $key
     * @return type
     */
    public static function config($key = NULL) {
        if (!self::$_initialized) {
            self::init();
        }
        return (isset($key) ? ((array_key_exists($key, self::$_settings)) ? self::$_settings[$key] : FALSE) : self::$_settings);
    }

    /**
     * Determines the current environment, and sets basic runtime parameters to 
     * help avoid common errors that result from environmental assumptions that 
     * are missing or not defined as expected.
     */
    private static function _setEnv() {
        
    }

    private static function _setLogger(array $settings = array(), array $flags = array()) {
        $settings = ((!empty($settings)) ? : self::_getDefaultSettings()['core']['log']);
        $logger = constant('self::LOGGER_' . strtoupper($settings['logmode']));
        self::$_logger = new $logger($settings, $flags);
    }

    private static function _getDefaultSettings() {
        $config = realpath(dirname(__FILE__) . DIRECTORY_SEPARATOR . self::SETTINGS_DEFAULT);

        if ($config) {
            $config = json_decode(file_get_contents($config), JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE | JSON_OBJECT_AS_ARRAY);
        }
        return $config;
    }

}
