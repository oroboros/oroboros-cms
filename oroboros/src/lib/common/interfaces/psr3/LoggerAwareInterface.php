<?php
namespace oroboros\src\lib\common\interfaces\psr3;
/**
 * @author PHP-FIG <http://php-fig.org>
 */
interface LoggerAwareInterface extends \Psr\Log\LoggerAwareInterface
{}