<?php
namespace oroboros\src\lib\common\interfaces\views;
/*
 * The MIT License
 *
 * Copyright 2016 Brian Dayhoff <brian@mopsyd.me>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 *
 * @author Brian Dayhoff <brian@mopsyd.me>
 */
interface ViewInterface {
    
    const CLASS_TYPE = "::view::";
    const FLAG_OUTPUT_PLAINTEXT = "::plaintext::";
    const FLAG_OUTPUT_HTML = "::html::";
    const FLAG_OUTPUT_CSS = "::css::";
    const FLAG_OUTPUT_JAVASCRIPT = "::javascript::";
    const FLAG_OUTPUT_EMAIL = "::email::";
    const FLAG_OUTPUT_RSS = "::rss::";
    const FLAG_OUTPUT_CRON = "::cron::";
    const FLAG_OUTPUT_ROBOTS = "::robots::";
    const FLAG_OUTPUT_SITEMAP = "::sitemap::";
    
    public function __construct(array $params = array(), array $flags = array());
    public function initialize(array $params = array(), array $flags = array());
    public function render(array $params = array(), array $flags = array());
    public function reset(array $params = array(), array $flags = array());
    public function redirect($location, $internal = 1);
}
