<?php

/*
 * The MIT License
 *
 * Copyright 2016 Brian Dayhoff <brian@mopsyd.me>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\src\lib\common\interfaces\api;

/**
 * <Oroboros Template API Interface>
 * This interface provides the public API for usage 
 * of html template classes in the Oroboros Core.
 * @author Brian Dayhoff <brian@mopsyd.me>
 * @since 0.0.2a
 */
interface TemplateApi extends \oroboros\src\lib\common\interfaces\BaseAbstractInterface {
    
    const CLASS_TYPE = "::template::";
    
    /**
     * <Short Tag Flag>
     * When passed to the tag generator, 
     * will produce a short tag (eg: <br>)
     * @note This is already done on void elements, it is not neccessary to pass this flag in that case.
     * @since 0.0.2a
     */
    const FLAG_SHORT_TAG = "::short-tag::";
    
    /**
     * <Inline Tag Flag>
     * When passed to the tag generator, 
     * will prevent entering a line break 
     * at the end of the tag. This prevents 
     * the inline-block render bug that adds 
     * the line break as a space in markup, 
     * and tends to break responsive layouts.
     * @since 0.0.2a
     */
    const FLAG_INLINE_TAG = "::inline-tag::";
    
    /**
     * <Template Constructor>
     * If [$params] is supplied, it will call [queue($params, $flags)] 
     * after construction is complete.
     * @param array $params (optional)
     * @param array $flags (optional>
     * @since 0.0.2a
     */
    public function __construct(array $params = array(), array $flags = array());

    /**
     * <Template Initialization>
     * Must be called before output. If [$params] is supplied, 
     * it will call [queue($params, $flags)] after initialization 
     * is complete.
     * @param array $params (optional)
     * @param array $flags (optional>
     * @return void
     * @since 0.0.2a
     */
    public function initialize(array $params = array(), array $flags = array());

    /**
     * <Template Render Method>
     * Directs the template to render fully, using the currently set data.
     * Initialization method must be called before this point. If [$params] 
     * is not empty, will call [queue($params, $flags)] before rendering.
     * @param array $params (optional)
     * @param array $flags (optional)
     * @return void
     * @since 0.0.2a
     */
    public function render(array $params = array(), array $flags = array());
    
    /**
     * <Template Master Reset Method>
     * Calls all individual reset methods. Restores template data to default 
     * settings. If [$params] is supplied, it will call [queue($params, $flags)] 
     * after the reset operation is complete.
     * @param array $params (optional)
     * @param array $flags (optional)
     * @return void
     * @since 0.0.2a
     */
    public function reset(array $params = array(), array $flags = array());

    /**
     * <Template Part Retrieval Method>
     * Returns a rendered template part. If data is needed to fill 
     * out the template, it should be supplied in [$params]. If any 
     * special conditions may change the way that the template part 
     * is laid out, it should be passed in [$flags].
     * @param string $part (required) The template part name.
     * @param array $params (optional)
     * @param array $flags (optional)
     * @return string The completed template markup
     * @since 0.0.2a
     */
    public function part($part, array $params = array(), array $flags = array());

    /**
     * <Template Bulk Queue Method>
     * Calls the setter method for any known data type that the 
     * template handles. When possible, queues additional data 
     * non-destructively. If [$params] is empty or not provided, 
     * has no effect. Queue values should be key => value pairs.
     * @param array $params (optional)
     * @param array $flags (optional)
     * @return void
     * @since 0.0.2a
     */
    public function queue(array $params = array(), array $flags = array());
    
    /**
     * <Getter Methods>
     * The following methods can be used in 
     * template sections to retrieve supplied 
     * page information individually.
     */
    
    public function author();
    public function description();
    public function title();
    public function lang();
    public function meta();
    public function pagename();
    public function favicon();
    public function fonts();
    public function scripts();
    public function css();
    public function heading();
    public function metadata();
    
    /**
     * <Setter Methods>
     * The following methods can be used by a View 
     * to set page information individually.
     */
    
    public function content($key);
    public function setAuthor($author);
    public function setDescription($description);
    public function setTitle($title);
    public function setLang($lang);
    public function setMeta($name, $meta);
    public function setPagename($id);
    public function setFavicon($name, $favicon);
    public function setFont($name, $font);
    public function setScript($name, $script);
    public function setCss($name, $stylesheet);
    public function setHeading($heading);
    public function setMetadata($name, $metadata);
    public function setContent($name, $content);
    
    /**
     * <Reset Methods>
     * The following methods can be used by a 
     * View to reset page information individually.
     */
    
    public function resetContent();
    public function resetMetadata();
    public function resetHeading();
    public function resetCss();
    public function resetScripts();
    public function resetFonts();
    public function resetFavicon();
    public function resetPagename();
    public function resetMeta();
    public function resetLang();
    public function resetTitle();
    public function resetAuthor();
    public function resetDescription();
}
