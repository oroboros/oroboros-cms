<?php
namespace oroboros\src\lib\common\interfaces\api;
/*
 * The MIT License
 *
 * Copyright 2016 Brian Dayhoff <brian@mopsyd.me>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * <Oroboros API>
 * This class exposes the primary Oroboros Core methods available 
 * through the global accessor.
 * @see \oroboros\Oroboros
 * @author Brian Dayhoff <brian@mopsyd.me>
 */
interface OroborosAPIInterface extends \oroboros\src\lib\common\interfaces\api\OroborosFlagInterface {
    
    /**
     * <Initialization>
     * Calls the initial
     * ------
     * If you ran the front controller routine, this is already done.
     * If you enabled unit testing, you need to do this manually.
     */
    public static function init(array $options = array(), array $flags = array());
    public static function filepath();
    
    /**
     * <Pre-Initialization Methods>
     * Don't call these, call the bootload routine!
     */
    
}
