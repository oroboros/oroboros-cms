<?php

/*
 * The MIT License
 *
 * Copyright 2016 Brian Dayhoff <brian@mopsyd.me>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\src\lib\common\views;

/**
 * Description of AjaxView
 *
 * @author Brian Dayhoff <brian@mopsyd.me>
 */
final class AjaxView extends \oroboros\src\lib\common\abstracts\views\AbstractView 
implements \oroboros\src\lib\common\interfaces\api\OroborosHTTPStatusCodeAPIInterface
{
    const DEFAULT_RESPONSE_TYPE = 'text';

    private $_request_types = array(
        'get' => self::FLAG_TYPE_GET,
        'post' => self::FLAG_TYPE_POST,
        'put' => self::FLAG_TYPE_PUT,
        'delete' => self::FLAG_TYPE_DELETE,
        'options' => self::FLAG_TYPE_OPTIONS,
        'head' => self::FLAG_TYPE_HEAD,
    );
    private $_request_type;
    private $_request;
    private $_response_types = array(
        'text',
        'json',
        'html',
        'scripts',
        'flash',
        'css'
    );
    private $_response_data = array();
    private $_response_headers = array();
    private $_response_type = self::DEFAULT_RESPONSE_TYPE;
    private $_encoding = array();
    private $_schema;
    private $_errors;
    
    public function __construct(array $params = array(), array $flags = array()) {
        parent::__construct($params, $flags);
        ob_start();
    }
    
    public function __destruct() {
        print ob_get_clean();
        parent::__destruct();
    }
    
    public function mode($mode = self::DEFAULT_RESPONSE_TYPE) {
        if (!in_array($mode, $this->_response_types)) {
            throw new \oroboros\src\lib\common\libraries\exception\OroborosException("Invalid render mode [" . $mode . "] specified for AJAX View.", self::ERROR_LOGIC_BAD_PARAMETERS);
        }
        $this->_response_type = $mode;
    }
    
    public function render(array $params = array(), array $flags = array()) {
        echo '{"view": "static"}';
        exit();
    }
}
