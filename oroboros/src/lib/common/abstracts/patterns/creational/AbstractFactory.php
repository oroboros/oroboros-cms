<?php
namespace oroboros\src\lib\common\abstracts\patterns\creational;
/*
 * The MIT License
 *
 * Copyright 2016 Brian Dayhoff <brian@mopsyd.me>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * <Oroboros Abstract Factory>
 * @note This is not the AbstractFactory design pattern. This is literally an abstraction of the Oroboros Factory.
 * @author Brian Dayhoff <brian@mopsyd.me>
 */
abstract class AbstractFactory extends \oroboros\src\lib\common\abstracts\OroborosBaseAbstract {
    
    private $_source;
    private $_sources_valid = array();
    
    public function __construct(array $params = array(), array $flags = array()) {
        parent::__construct($params, $flags);
    }
    
    public function __destruct() {
        parent::__destruct();
    }
    
    public function initialize(array $params = array(), array $flags = array()) {
        parent::initialize($params, $flags);
    }
    
    public function load($type, array $params = array(), array $flags = array()) {
        $namespace = $this->_getSourceResource() . $type;
        $class = new $namespace();
        $class->initialize($params, $flags);
        return $class;
    }
    
    public function fetch($source, $type, array $params = array(), array $flags = array()) {
        return $this->source($source)->load($type, $params, $flags);
    }
    
    public function source($source) {
        if (!$this->_checkSource($source)) {
            throw new \oroboros\src\lib\common\libraries\exception\OroborosException("Invalid factory source [" . (string) $source . "]", self::ERROR_LOGIC_BAD_PARAMETERS);
        }
        $this->_setSource($source);
        return $this;
    }
    
    protected function _validSources(array $sources) {
        $this->_setValidSources($sources);
    }
    
    protected function _getSourceResource() {
        return $this->_sources_valid[$this->_source];
    }
    
    protected function _checkSource($source) {
        return (array_key_exists($source, $this->_sources_valid));
    }
    
    private function _setSource($source) {
        $this->_source = $source;
    }
    
    private function _setValidSources(array $sources) {
        $this->_sources_valid = $sources;
    }
}
