<?php
namespace oroboros\src\lib\common\abstracts\models;
/*
 * The MIT License
 *
 * Copyright 2016 Brian Dayhoff <brian@mopsyd.me>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * <Oroboros Abstract Model>
 * @author Brian Dayhoff <brian@mopsyd.me>
 */
abstract class AbstractModel 
extends \oroboros\src\lib\common\abstracts\OroborosBaseAbstract 
implements \oroboros\src\lib\common\interfaces\psr3\LoggerAwareInterface  
{
    
    use \oroboros\src\lib\common\traits\utilities\LoaderTrait;
    use \oroboros\src\lib\common\traits\utilities\DefaultLoggerTrait;
    
    /**
     * These are the types of resources that can be loaded with the $this->load method
     * @var array $_allowed_loader_types 
     */
    private $_allowed_loader_types = array(
        "model",
        "library",
        "module",
        "app"
    );
    
    public function __construct(array $params = array(), array $flags = array()) {
        parent::__construct($params, $flags);
        $this->_setAllowedLoaderTypes($this->_allowed_loader_types);
    }
    
    public function __destruct() {
        parent::__destruct();
    }
    
    public function initialize(array $params = array(), array $flags = array()) {
        parent::initialize($params, $flags);
        $this->_initializeLoader();
    }
    
    protected function _updateParameterConditions($parameter_key) {
            $name = str_replace(' ', NULL, ucwords(str_replace('-', ' ', $parameter_key)));
            if (method_exists($this, '_reset' .$name)) {
                $this->{'_reset' . $name}();
            }
            if (method_exists($this, '_set' .$name)) {
                $this->{'_set' . $name}($this->_getParam($parameter_key));
            }
    }
    
}