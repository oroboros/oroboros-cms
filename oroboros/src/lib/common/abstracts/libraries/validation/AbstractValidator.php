<?php

/*
 * The MIT License
 *
 * Copyright 2016 Brian Dayhoff <brian@mopsyd.me>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\src\lib\common\abstracts\libraries\validation;

/**
 * Description of AbstractValidator
 *
 * @author Brian Dayhoff <brian@mopsyd.me>
 */
abstract class AbstractValidator extends \oroboros\src\lib\common\abstracts\libraries\AbstractLibrary {

    const DEFAULT_WORKER = FALSE;

    private $_workers = array(
        'database' => 'workers\\DatabaseValidator'
    );
    private $_worker;

    public function __construct(array $params = array(), array $flags = array()) {
        if (self::DEFAULT_WORKER) {
            $this->_registerWorker('default', self::DEFAULT_WORKER);
            $this->_worker = $this->_getWorker('default');
        }
        parent::__construct($params, $flags);
    }

    public function initialize(array $params = array(), array $flags = array()) {
        if (array_key_exists('workers', $params)) {
            $this->_registerWorkers($params['workers']);
            unset($params['workers']);
        }
        if (self::DEFAULT_WORKER) {
            $this->_worker = $this->_getWorker(self::DEFAULT_WORKER);
        } elseif (array_key_exists('mode', $params)) {
            $this->_worker = $this->_getWorker($params['mode']);
        }
        parent::initialize($params, $flags);
    }
    
    public function validate($data, array $schema) {
        return $this->_worker->validate($data, $schema);
    }

    public function mode($mode) {
        $this->_worker = $this->_getWorker($mode);
        return $this;
    }

    protected function _registerWorker($worker, $resource) {
        $this->_workers[$worker] = $resource;
    }

    protected function _registerWorkers(array $workers) {
        foreach ($workers as $worker => $resource) {
            $this->_registerWorker($worker, $resource);
        }
    }

    private function _getWorker($worker = NULL) {
        if (!isset($worker)) {
            return $this->_worker;
        }
        if (!array_key_exists($worker, $this->_workers)) {
            throw new \oroboros\src\lib\common\libraries\exception\OroborosException("Invalid validation worker: [" . (string) $worker . "]", self::ERROR_LOGIC_BAD_PARAMETERS);
        }
        try {
            $namespace = substr(get_class($this), 0, strrpos(get_class($this), '\\'));
            $class = $namespace . '\\' . $this->_workers[$worker];
            $workerObject = new $class();
            return $workerObject;
        } catch (\Exception $e) {
            $this->_log(self::WARNING, "Failed to load validation worker: [" . (string) $worker . ']' . PHP_EOL . '-->Referenced in [:: ' . get_class($this) . ' ::]' . PHP_EOL . '-->[:: Backtrace ::]' . PHP_EOL . '--> ' . $e->getTraceAsString() . PHP_EOL);
            throw new \oroboros\src\lib\common\libraries\exception\OroborosException("Could not load worker: [" . (string) $worker . "]", self::ERROR_LOGIC, $e);
        }
    }

}
