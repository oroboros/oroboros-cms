<?php
namespace oroboros\src\lib\common\abstracts\libraries;
/*
 * The MIT License
 *
 * Copyright 2016 Brian Dayhoff <brian@mopsyd.me>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * Description of AbstractWorker
 *
 * @author Brian Dayhoff <brian@mopsyd.me>
 */
class AbstractWorker 
extends \oroboros\src\lib\common\abstracts\libraries\AbstractLibrary 
implements \oroboros\src\lib\common\interfaces\libraries\WorkerInterface 
{
    
    private $_schema = NULL;
    private $_state;
    private $_valid_states = array();
    
    public function __construct(array $params = array(), array $flags = array()) {
        parent::__construct($params, $flags);
    }
    
    public function initialize(array $params = array(), array $flags = array()) {
        parent::initialize($params, $flags);
        $schema = ((isset($params['schema'])) ? $params['schema'] : new \stdClass());
        $conditions = ((isset($params['conditions'])) ? $params['schema'] : new \stdClass());
        if (!is_object($schema)) {
            throw new \oroboros\src\lib\common\libraries\exception\OroborosException("[ERROR] Malformed schema passed to [" . __CLASS__ . "]!", self::ERROR_LOGIC_BAD_PARAMETERS);
        }
    }
    
    /**
     * Null validation step
     * Silences validation. Overrule in a child class for conditional validation.
     * @param mixed $data
     * @return boolean (always true)
     */
    public function validate($data, array $schema = array()) {
        return TRUE;
    }
    
    /**
     * Nulled worker execution
     * Overrule in a child class to add worker execution tasks as needed.
     */
    public function execute() {}
    
    protected function _updateParameterConditions($parameter_key) {
            $name = str_replace(' ', NULL, ucwords(str_replace('-', ' ', $parameter_key)));
            if (method_exists($this, '_reset' .$name)) {
                $this->{'_reset' . $name}();
            }
            if (method_exists($this, '_set' .$name)) {
                $this->{'_set' . $name}($this->_getParam($parameter_key));
            }
    }
    
    /**
     * Default schema setter
     * @param \stdClass $schema
     */
    protected function _setSchema(\stdClass $schema) {
        $this->_schema = $schema;
    }
    
    protected function _setConditions($conditions) {
        $this->_conditions = $conditions;
    }
    
    /**
     * Default schema getter
     * returns either the entire schema (if $part=null), 
     * the specified schema index, or false if specified and not found.
     * @param string $part
     * @return mixed
     */
    protected function _schema($part = NULL) {
        return ((isset($part)) 
                ? ((isset($this->_schema->{(string) $part})) 
                    ? $this->_schema->{(string) $part} 
                    : FALSE) 
                : $this->_schema);
    }
    
    final protected function _checkInitialization() {
        if (!$this->isInitialized()) {
            throw new \OroborosInstallerException("[ERROR] Installation worker [" . __CLASS__ . "] is not initialized!", self::ERROR_INITIALIZATION);
        }
    }
    
    protected function _state($state = NULL) {
        //return the current state if no parameter is specified
        if (!isset($state)) {
            return $this->_state;
        }
        //only causes failure on state change operation
        if (empty($this->_valid_states)) {
            throw new \OroborosInstallerException("[WARNING] Valid worker states must be registered before the state can be changed.", 1605);
        }
        //check if attempting to pass an invalid state
        if (!in_array($state, $this->_valid_states)) {
            throw new \OroborosInstallerException('[ERROR] Invalid worker state: ' . (string) $state . '!', 1655);
        }
        $this->_state = $state;
        $this->_onStateChange();
    }
    
    /**
     * Registers a list of valid states.
     * @param array|object $states Must be an array or iterable object
     * @return void
     */
    protected function _registerStates($states) {
        foreach ($states as $key => $value) {
            if (!in_array($value, $this->_valid_states)) {
                $this->_valid_states[] = $value;
            }
        }
    }
    
    /**
     * Removes all current valid states. Existing state will remain frozen while
     * no valid states are present, but will not otherwise change.
     */
    protected function _clearStates() {
        $this->_valid_states = array();
    }
    
    /**
     * Null state change.
     * Overrule this method in a child class to 
     * activate state pattern awareness as needed.
     * @return void
     */
    protected function _onStateChange() {}
}
