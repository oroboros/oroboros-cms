<?php
namespace oroboros\src\lib\common\abstracts;

/*
 * The MIT License
 *
 * Copyright 2016 Brian Dayhoff <brian@mopsyd.me>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * <Oroboros Base Abstract Class>
 * This class defines a few very simple configurations to identify 
 * as part of Oroboros, and implements common interfaces. All other 
 * Oroboros classes descend from this one wherever possible. There 
 * is a also a trait to emulate this behavior for classes that must 
 * extend a different class for some reason.
 * @see \oroboros\src\lib\common\traits\OroborosBaseTrait
 * @author Brian Dayhoff <brian@mopsyd.me>
 * @since 0.0.1a
 */
abstract class OroborosBaseAbstract 
implements \oroboros\src\lib\common\interfaces\BaseAbstractInterface 
{
    /**
     * <Instance Fingerprint Registry>
     * This is a list of all currently registered class fingerprints. 
     * It is only accessible to this class. This is used to insure 
     * that there are no collisions in instance keys for any reason 
     * (without needing to implement a heavier randomization engine), 
     * and otherwise has no effect.
     * @var array $_fingerprint_instances 
     */
    private static $_fingerprint_instances = array();

    /**
     * <Object Instance Fingerprint>
     * This is generated at the end of the base class constructor sequence. 
     * It is a unique key that can only exist for that specific instance of 
     * a constructor firing.
     * @var string $_fingerprint 
     */
    private $_fingerprint;

    /**
     * <Default Class API Definition>
     * Override this in child classes to define an API for the class. 
     * If you have an api, it should be a valid API interface. 
     * @note API interfaces MUST declare the following class constants (all may be false by default): [DOCS, NAME, SDK, REPO, VERSION, AUTHOR, LICENSE, SITE];
     * @see \oroboros\src\lib\common\interfaces\api\DefaultApiInterface
     * @note DO NOT DO THIS IN AN INTERFACE OR YOU WILL WHITESCREEN.
     */
    const API = FALSE;

    private $_initialized = FALSE;
    private $_constructed = FALSE;
    private $_parameters = array();
    private $_default_parameters = array();
    private $_parameters_initialized = FALSE;
    private $_flags = array();
    private $_default_flags = array();
    private $_flags_initialized = FALSE;
    private $_locks = array(
        "read" => FALSE, //disables reading public variables
        "write" => FALSE, //disables setting public variables
        "execute" => FALSE, //disables any methods that use $this->_checkLockStatus("execute");
    );

    /**
     * <Default Object Constructor>
     * All non-utility classes in the oroboros system 
     * use this constructor. 
     * Utility classes are considered to be any class 
     * that extends from a non-core class.
     * @param type $params
     * @param type $flags
     * @return void
     * @since 0.0.2a
     */
    public function __construct(array $params = array(), array $flags = array()) {
        $this->_handleParameters($params);
        $this->_handleFlags($flags);
        $this->_setFingerprint();
        $this->_constructed = TRUE;
    }

    /**
     * <Default Destructor>
     * No operation.
     * @since 0.0.1a
     */
    public function __destruct() {
        //no-op
    }

    /**
     * <Default Magic Getter>
     * Defers magic method getter to the standard getter method
     * @param string $name
     * @return type
     * @since 0.0.1a
     */
    public function __get($name) {
        return $this->get($name);
    }

    /**
     * <Default Magic Setter>
     * Defers magic method setter to the standard setter method
     * @param type $name
     * @param type $value
     * @return type
     * @since 0.0.1a
     */
    public function __set($name, $value) {
        return $this->set($name, $value);
    }

    /**
     * <Default Setter>
     * Default standard setter. Performs the same 
     * as the native php getter. Provided so child 
     * classes do not need to the magic getter.
     * @param type $key
     * @param mixed $value
     * @return boolean
     * @since 0.0.1a
     */
    public function set($key, $value) {
        if ($this->_checkLock('write')) {
            throw new \oroboros\src\lib\common\libraries\exception\OroborosException("Cannot set value [$key] when object write access is disabled.", self::ERROR_SECURITY_LOCKED_RESOURCE);
        }
        return $this->{$key} = $value;
    }

    /**
     * <Default Getter>
     * Default standard getter. Throws an oroboros 
     * exception if resource not found instead of an 
     * error. Otherwise behaves the same as the 
     * native php getter.
     * 
     * @param string $key
     * @return mixed
     * @throws \oroboros\src\lib\common\libraries\exception\OroborosException
     * @since 0.0.1a
     */
    public function get($key) {
        if ($this->_checkLock('read')) {
            throw new \oroboros\src\lib\common\libraries\exception\OroborosException("Cannot set value [$key] when object read access is disabled.", self::ERROR_SECURITY_LOCKED_RESOURCE);
        }
        return ((isset($this->{$key})) ? $this->{$key} : NULL);
    }

    /**
     * <Default Initialization Method>
     * Provides an unopinionated way to initialize parameters and flags. 
     * This class does nothing with the parameters or flags aside from 
     * storing them so they can be referenced and checked against. Child 
     * classes should implement their own functionality around this, but 
     * it can be safely ignored entirely.
     * @note Parameter and flag initialization can also happen directly in the constructor if they need to be exposed before your class initialization method runs, or may be exposed at both if you need to separate pre-initialization parameters from initialization parameters.
     * @param type $params
     * @param type $flags
     * @since 0.0.1a
     */
    public function initialize(array $params = array(), array $flags = array()) {
        $this->_handleParameters($params);
        $this->_handleFlags($flags);
        $this->_initialized = TRUE;
    }

    /**
     * <Default Initialization Check Method>
     * Provides a simple way of checking if initialization has occurred. 
     * If initialization method has been called, this will return true.
     * @return boolean
     * @since 0.0.2a
     */
    public function isInitialized() {
        return $this->_initialized;
    }

    /**
     * <Fingerprint Getter Method>
     * This returns the unique fingerprint of the object. 
     * All instantiated Oroboros objects have this method. 
     * This method may not be overruled.
     * @return type
     * @final
     * @since 0.0.2a
     */
    final public function fingerprint() {
        return $this->_fingerprint;
    }
    
    /**
     * <Is Base Class Constructor Complete Check Method>
     * Returns [true] if the base class has been constructed. 
     * Useful for control blocks that suppress triggers from 
     * firing before construction is complete.
     * @return boolean
     * @since 0.0.2a
     */
    protected function _isConstructed() {
        return $this->_constructed;
    }

    /**
     * <Locking Functionality>
     * Oroboros provides a basic asset locking mechanism to 
     * add additional security to your class. This allows you 
     * to designate the class as read-only, write-only, or to 
     * customize which functions are allowed to run based on 
     * state. All of this is disabled by default, but is 
     * provided for convenience.
     */

    /**
     * <Default Master Lock Check Method>
     * Provides a simple way of checking all current lock types at once.
     * @param array $status
     * @return type
     * @throws \OroborosException
     * @since 0.0.1a
     */
    protected function _checkLocks(array $status) {
        if (!in_array($status, array_keys($this->_locks))) {
            throw new \OroborosException();
        }
        return array(
            "read" => $this->_checkLock('read'),
            "write" => $this->_checkLock('write'),
            "execute" => $this->_checkLock('execute')
        );
    }

    /**
     * <Default Lock Checking Method>
     * Checks whether the provided type is enabled 
     * or disabled. If the type is not recognized, 
     * throws an exception.
     * @param type $type
     * @return type
     * @throws \oroboros\src\lib\common\libraries\exception\OroborosException
     * @since 0.0.1a
     */
    protected function _checkLock($type) {
        //acceptible $type = ["read", "write", "execute"]
        if (!in_array($type, array_keys($this->_locks))) {
            throw new \oroboros\src\lib\common\libraries\exception\OroborosException("Invalid lock type. Acceptable lock types are [" . implode(array_keys($this->_locks)) . "]", self::ERROR_LOGIC_BAD_PARAMETERS);
        }
        return $this->_locks[$type];
    }

    /**
     * <Default Lock Setter Method>
     * Sets a lock to the specified state (true is on, false is off).
     * @param string $type ["read", "write", "execute"]
     * @param boolean $value [true, false, 0, 1]
     * @return type
     * @throws \oroboros\src\lib\common\libraries\exception\OroborosException
     * @since 0.0.1a
     */
    protected function _setLock($type, $value) {
        //acceptible $type = ["read", "write", "execute"]
        //acceptible $value = [true, false, 0, 1]
        if (!in_array($type, array_keys($this->_locks))) {
            throw new \oroboros\src\lib\common\libraries\exception\OroborosException("Invalid lock type. Acceptable lock types are [" . implode(array_keys($this->_locks)) . "]", self::ERROR_LOGIC_BAD_PARAMETERS);
        }
        if (!in_array($value, array(TRUE, FALSE, 0, 1))) {
            throw new \oroboros\src\lib\common\libraries\exception\OroborosException("Invalid lock value. Acceptable lock types are [true, false, 0, 1]", self::ERROR_LOGIC_BAD_PARAMETERS);
        }
        return $this->_locks[$type];
    }

    /**
     * <Parameter Handling Null Template>
     * Overrule this method with how your parameters passed 
     * into the constructor or initialization method should 
     * be handled. It will be called whenever those parameters 
     * are updated.
     * @return void
     * @since 0.0.1a
     */
    protected function _updateParameterConditions($parameter_key) {
        
    }

    /**
     * <Flag Handling Null Template>
     * Overrule this method with how your flags passed 
     * into the constructor or initialization method should 
     * be handled. It will be called whenever those parameters 
     * are updated.
     * @return void
     * @since 0.0.1a
     */
    protected function _updateFlagConditions() {
        
    }
    
    /**
     * <Default Flag Setter Method>
     * Sets a flag.
     * @param type $key
     * @throws \oroboros\src\lib\common\libraries\exception\OroborosException
     * @since 0.0.1a
     */
    protected function _setFlag($key) {
        if (!is_string($flag)) {
            throw new \oroboros\src\lib\common\libraries\exception\OroborosException("Invalid object key: [$key] requested from [" . get_class($this) . '], thrown at ' . __LINE__ . ' of ' . __METHOD__, self::ERROR_LOGIC_BAD_PARAMETERS);
        }
        if (array_search($key, $this->_flags) !== FALSE) {
            $this->_flags[] = $key;
        }
    }

    /**
     * <Default Flag Check Method>
     * Checks if a specific flag has been set.
     * @param string $flag
     * @return boolean
     * @since 0.0.1a
     */
    protected function _checkFlag($flag) {
        return (in_array($flag, $this->_flags));
    }
    
    /**
     * <Default Parameter Check Method>
     * Returns [TRUE] if a parameter is defined, [FALSE] if undefined. 
     * This can be used to circumvent exceptions if the presence of a 
     * specific parameter is not certain.
     * @param string $key
     * @return boolean
     * @since 0.0.2a
     */
    protected function _checkParam($key) {
        return (array_key_exists($key, $this->_parameters));
    }
    
    /**
     * <Default Parameter Getter Method>
     * Gets a specific parameter by key.
     * @param string $key
     * @return mixed
     * @throws \oroboros\src\lib\common\libraries\exception\OroborosException
     */
    protected function _getParam($key) {
        return ((isset($this->_parameters[$key])) ? $this->_parameters[$key] : NULL);
    }

    /**
     * <Default Parameter Setter Method>
     * Sets a specific parameter.
     * @param string $key
     * @param mixed $value
     * @return void
     * @since 0.0.1a
     */
    protected function _setParam($key, $value) {
        $this->_parameters[$key] = $value;
    }
    
    /**
     * <Default Parameter List Setter Method>
     * Sets a list of parameters.
     * @param array $params
     * @return void
     * @since 0.0.2a
     */
    protected function _setParams(array $params) {
        array_map(function($param) use ($params) {
            return $this->_setParam(array_search($param, $params), $param);
        }, $params);
    }

    /**
     * <Internal Methods>
     * Under the hood stuff
     */

    /**
     * If [$parameters] is provided, sets the object 
     * flags to the provided values, and calls 
     * the _updateParameterConditions method.
     * If no parameters provided, Sets the parameters 
     * back to their default state, and 
     * calls the _updateParametersConditions method
     * (which fires any time parameters are updated).
     * @param array $parameters
     */
    private function _handleParameters(array $parameters) {
        if (empty($parameters)) {
            $parameters = $this->_default_parameters;
        }
        foreach ($parameters as $key => $value) {
            $this->_parameters[$key] = $value;
            $this->_updateParameterConditions($key);
        }
        $this->_parameters_initialized = TRUE;
    }

    /**
     * If [$flag] is provided, sets the object 
     * flags to the provided values, and calls 
     * the _updateFlagConditions method.
     * If no parameters provided, Sets the flags 
     * back to their default state, and 
     * calls the _updateFlagConditions method
     * (which fires any time flags are updated).
     */
    private function _handleFlags(array $flags) {
        if (empty($flags)) {
            $parameters = $this->_default_flags;
        }
        $this->_flags = $flags;
        $this->_flags_initialized = TRUE;
        $this->_updateFlagConditions();
    }

    /**
     * Resets the defined class parameters to their default state.
     * @return void
     * @since 0.0.1a
     */
    private function _resetParameters() {
        $this->_parameters_initialized = FALSE;
        $this->_handleParameters($this->_default_parameters);
    }

    /**
     * Resets flags in the object back to their predefined defaults.
     * @return void
     * @since 0.0.1a
     */
    private function _resetFlags() {
        $this->_flags_initialized = FALSE;
        $this->_handleFlags($this->_default_flags);
    }

    /**
     * Sets a group of flags.
     * @param array $flags
     * @since 0.0.2a
     */
    protected function _setFlags(array $flags) {
        $this->_handleFlags($flags);
    }

    /**
     * Places a lock on this object instance for 
     * one of the specified lock types [read, write, execute].
     * @param type $type
     * @throws \oroboros\src\lib\common\libraries\exception\OroborosException
     * @since 0.0.1a
     */
    private function _lock($type) {
        //acceptible $type = ["read", "write", "execute"]
        if (!in_array($type, array_keys($this->_locks))) {
            throw new \oroboros\src\lib\common\libraries\exception\OroborosException("Invalid lock type. Acceptable lock types are [" . implode(array_keys($this->_locks)) . "]", self::ERROR_LOGIC_BAD_PARAMETERS);
        }
        $this->_locks[$type] = TRUE;
    }

    /**
     * Unlocks access in the class for a specific 
     * lock type [read, write, execute]
     * @param type $type
     * @throws \oroboros\src\lib\common\libraries\exception\OroborosException
     * @since 0.0.1a
     */
    private function _unlock($type) {
        //acceptible $type = ["read", "write", "execute"]
        if (!in_array($type, array_keys($this->_locks))) {
            throw new \oroboros\src\lib\common\libraries\exception\OroborosException("Invalid lock type. Acceptable lock types are [" . implode(array_keys($this->_locks)) . "]", self::ERROR_LOGIC_BAD_PARAMETERS);
        }
        $this->_locks[$type] = FALSE;
    }

    /**
     * Creates a unique fingerprint string.
     * @return string
     * @since 0.0.2a
     */
    private function _generateObjectFingerprint() {
        $time = \time();
        $class = \get_class($this);
        $rand = (string) \mt_rand(0, \mt_getrandmax());
        $fingerprint = \sha1($class . $time . $rand);
        //If for some reason a collision occurs, 
        //new fingerprints will be attempted until 
        //a unique one is found. The likelihood of 
        //collision is low enough that this recursion 
        //should not cause an issue.
        return array_key_exists($fingerprint, self::$_fingerprint_instances) ? $this->_generateObjectFingerprint() : $fingerprint;
    }

    /**
     * Sets a unique fingerprint for each 
     * instantiated instance of a class.
     * @return void
     * @since 0.0.2a
     */
    private function _setFingerprint() {
        $fingerprint = $this->_generateObjectFingerprint();
        $this->_fingerprint = $fingerprint;
    }

}
