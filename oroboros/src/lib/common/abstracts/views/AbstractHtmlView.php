<?php

namespace oroboros\src\lib\common\abstracts\views;

/*
 * The MIT License
 *
 * Copyright 2016 Brian Dayhoff <brian@mopsyd.me>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * <Oroboros Abstract HTML View>
 *
 * @author Brian Dayhoff <brian@mopsyd.me>
 */
abstract class AbstractHtmlView extends \oroboros\src\lib\common\abstracts\views\AbstractView implements \oroboros\src\lib\common\interfaces\api\OroborosHTTPStatusCodeAPIInterface {

    const CONTENT_TYPE = 'text/html; charset=UTF-8';
    const DEFAULT_TEMPLATE = 'install\\InstallTemplate';
    const DEFAULT_TEMPLATE_SOURCE = 'system';

    private $_headers = array();
    private $_meta = array();
    private $_css = array();
    private $_scripts = array();
    private $_fonts = array();
    private $_content = array();
    private $_template;
    private $_template_queue;

    public function __construct(array $params = array(), array $flags = array()) {
        parent::__construct($params, $flags);
    }

    public function initialize(array $params = array(), array $flags = array()) {
        if (isset($params['template'])) {
            $template = $params['template'];
            unset($params['template']);
        } else {
            $template = self::DEFAULT_TEMPLATE;
        }
        parent::initialize($params, $flags);
        $template = $this->_load('template', self::DEFAULT_TEMPLATE_SOURCE, $template, $params, $flags);
        $this->_checkTemplate($template);
    }

    protected function _buildContent(array $params = array(), array $flags = array()) {
        $template = $this->_template();
        $template->queue($this->_templateData());
        $template->render($params, $flags);
    }

    protected function _template() {
        return $this->_template;
    }

    protected function _resetTemplate() {
        $this->_template = $this->_default_template;
    }

    protected function _checkTemplate($template, array $flags = array()) {
        if (is_string($template)) {
            try {
                $template = $this->_load('template', self::DEFAULT_TEMPLATE_SOURCE, $template, array(), $flags);
                if (!($template instanceof \oroboros\src\lib\common\abstracts\libraries\template\TemplateAbstract)) {
                    throw new \oroboros\src\lib\common\libraries\exception\OroborosException("", self::ERROR_VIEW);
                }
                $template->initialize(array(), $flags);
                $this->_setTemplate($template);
                return TRUE;
            } catch (\Exception $e) {
                $this->_log(SELF::WARNING, "Invalid template supplied to view: [" . $template . "]", array('TRACE' => $e->getTraceAsString()));
                return FALSE;
            }
        } elseif (is_object($template) && ($template instanceof \oroboros\src\lib\common\abstracts\libraries\template\TemplateAbstract)) {
            $template->initialize(array(), $flags);
            $this->_setTemplate($template);
            return TRUE;
        } else {
            return FALSE;
        }
    }

    protected function _setTemplate(\oroboros\src\lib\common\abstracts\libraries\template\TemplateAbstract $template) {
        $this->_template = $template;
    }

    protected function _queueTemplateData(array $params = array(), array $flags = array()) {
        foreach ($params as $key => $value) {
            $this->_template_queue[$key] = $value;
        }
    }

    protected function _resetTemplateData() {
        $this->_template_queue = array();
    }

    protected function _templateData() {
        return $this->_template_queue;
    }

    protected function _resetScripts() {
        $this->_scripts = array();
    }

    protected function _setScripts(array $scripts) {
        foreach ($scripts as $key => $src) {
            $this->_scripts[$key] = $src;
        }
    }

    protected function _resetStylesheets() {
        $this->_css = array();
    }

    protected function _setStylesheets(array $stylesheets) {
        foreach ($stylesheets as $key => $src) {
            $this->_css[$key] = $src;
        }
    }

    protected function _resetMeta() {
        $this->_meta = array();
    }

    protected function _setMeta(array $meta) {
        foreach ($meta as $key => $value) {
            $this->_meta[$key] = $value;
        }
    }

    protected function _resetFonts() {
        $this->_fonts = array();
    }

    protected function _setFonts(array $fonts) {
        foreach ($fonts as $key => $src) {
            $this->_fonts[$key] = $src;
        }
    }

}
