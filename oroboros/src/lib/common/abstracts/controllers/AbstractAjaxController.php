<?php

namespace oroboros\src\lib\common\abstracts\controllers;

/*
 * The MIT License
 *
 * Copyright 2016 Brian Dayhoff <brian@mopsyd.me>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * <Oroboros Abstract Ajax Controller>
 * @author Brian Dayhoff <brian@mopsyd.me>
 * @since 0.0.2a
 */
abstract class AbstractAjaxController 
extends \oroboros\src\lib\common\abstracts\controllers\AbstractController 
implements \oroboros\src\lib\common\interfaces\controllers\OroborosControllerInterface 
{

    const DEFAULT_RESPONSE_TYPE = 'text';

    private $_request_types = array(
        'get' => self::FLAG_TYPE_GET,
        'post' => self::FLAG_TYPE_POST,
        'put' => self::FLAG_TYPE_PUT,
        'delete' => self::FLAG_TYPE_DELETE,
        'options' => self::FLAG_TYPE_OPTIONS,
        'head' => self::FLAG_TYPE_HEAD,
    );
    private $_request_type;
    private $_request;
    private $_response_types = array(
        'text',
        'json',
        'html',
        'scripts',
        'flash',
        'css'
    );
    private $_response_data = array();
    private $_response_headers = array();
    private $_response_type = self::DEFAULT_RESPONSE_TYPE;
    private $_encoding = array();
    private $_config;
    private $_preferences;
    private $_errors;

    public function initialize(array $params = array(), array $flags = array()) {
        parent::initialize($params, $flags);
        $this->_initializeAjaxEnvironment();
    }

    public function error(array $params = array(), array $flags = array()) {
        $trace = debug_backtrace();
        foreach($trace as $key => $value) {
            echo $value['function'] . PHP_EOL;
        }
        $code = ((isset($params['code'])) ? $params['code'] : self::DEFAULT_ERROR_CODE);
        http_response_code($code);
        exit();
    }
    
    protected function _queue($type, $key, $value = NULL) {
        switch ($type) {
            case 'header':
                return $this->_queueHeader($key, $value);
                break;
            case 'data':
                return $this->_queueData($key, $value);
                break;
            case 'default':
                throw new \oroboros\src\lib\common\libraries\exception\OroborosException("Invalid queue type: [" . $type . "]", self::ERROR_LOGIC_BAD_PARAMETERS);
                break;
        }
    }
    
    protected function _request() {
        return ((isset($this->_request['data'])) ? json_decode($this->_request['data'], JSON_OBJECT_AS_ARRAY): FALSE);
    }
    
    protected function _requestType() {
        return $this->_request_type;
    }

    protected function _encoding() {
        return $this->_encoding;
    }

    protected function _responseType() {
        return $this->_response_type;
    }
    
    protected function _packageRenderData(array $data = array()) {
        $result = array(
            'headers' => $this->_headers(),
            'data' => $this->_data(),
            'type' => $this->_responseType(),
            'schema' => $this->_config(),
            'errors' => $this->_errors(),
            'conditions' => $this->_conditions(),
            'router' => $this->_router()
        );
        /**
         * Non-destructively expose the View to contents of the render 
         * initialization param, if it exists.
         */
        
        if ($this->_checkParam('render')) {
            foreach ($this->_getParam('render') as $key => $value) {
                $result[$key] = $value;
            }
        }
        //overrides preset data, if defined and not empty.
        foreach ($data as $key => $value) {
            $result[$key] = $value;
        }
        return $result;
    }
    
    protected function _setRouter($router) {
        $this->_router = $router;
    }
    
    protected function _router() {
        return $this->_router;
    }
    
    protected function _setConditions($conditions) {
        $this->_conditions = $conditions;
    }
    
    protected function _conditions() {
        return $this->_conditions;
    }
    
    protected function _setErrors($errors) {
        $this->_errors = $errors;
    }
    
    protected function _errors() {
        return $this->_errors;
    }
    
    protected function _setConfig($config) {
        $this->_config = $config;
    }
    
    protected function _config($key = NULL) {
        if (isset($key)) {
            return ((isset($this->_config[$key]) ? $this->_config[$key] : FALSE));
        } else {
            return $this->_config;
        }
    }
    
    protected function _setPreferences($preferences) {
        $this->_preferences = $preferences;
    }
    
    protected function _preferences($key = NULL) {
        if (isset($key)) {
            return ((isset($this->_preferences[$key]) ? $this->_preferences[$key] : FALSE));
        } else {
            return $this->_preferences;
        }
    }
    
    protected function _setHeader($key, $value) {
        $this->_response_headers[$key] = $value;
    }
    
    protected function _headers() {
        return $this->_response_headers;
    }
    
    protected function _setData($key, $value) {
        $this->_response_data[$key] = $value;
    }
    
    protected function _data() {
        return $this->_response_data;
    }
    
    private function _initializeAjaxEnvironment() {
        $headers = getallheaders();
        $this->_initializeContentType($headers['Accept']);
        $this->_initializeCompression($headers['Accept-Encoding']);
        $this->_initializeRequestType();
    }

    private function _initializeContentType($content_type) {
        $type = FALSE;
        switch (explode(';', $content_type)[0]) {
            case 'text/html':
            case 'application/xhtml+xml':
                $type = 'html';
                break;
            case 'application/xml':
                $type = 'xml';
                break;
            case 'application/javascript':
                $type = 'scripts';
                break;
            case 'application/json':
                $type = 'json';
                break;
            case 'text/css':
                $type = 'css';
                break;
            case 'application/x-shockwave-flash':
                $type = 'flash';
                break;
            case 'text/plain':
            case '*/*':
            default:
                $type = self::DEFAULT_RESPONSE_TYPE;
                break;
        }
        $this->_response_type = $type;
    }

    private function _initializeCompression($encoding) {
        $encoding = explode(', ', $encoding);
        $this->_encoding = \oroboros\Common::map($encoding, function($item) {
                    return trim($item);
                });
    }

    private function _initializeRequestType() {
        $method = strtolower($_SERVER['REQUEST_METHOD']);
        $this->_request_type = $method;
        switch ($method) {
            case 'get':
                $this->_request = $_GET;
                break;
            case 'post':
                $this->_request = $_POST;
                break;
            case 'put':
            case 'delete':
                $putfp = fopen('php://input', 'r');
                $this->_request = '';
                while ($data = fread($putfp, 1024))
                    $this->_request .= $data;
                fclose($putfp);
                break;
        }
    }
}
