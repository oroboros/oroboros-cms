<?php

namespace oroboros\src\lib\common\libraries\logger;

/*
 * The MIT License
 *
 * Copyright 2016 Brian Dayhoff <brian@mopsyd.me>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * <Oroboros File Logger>
 * @note PSR-3 Compliant
 * This logger will write to a specified file in the logs directory.
 * @author Brian Dayhoff <brian@mopsyd.me>
 * @since 0.0.1a
 */
final class FileLogger extends \oroboros\src\lib\common\libraries\logger\AbstractLogger {

    const FLAG_TRUNCATE = '::truncate_logfile::';
    const LOGMODE = 'file';

    private $_pointer;
    private $_flags;
    private $_params;
    private $_write_mode;
    private $_file_path;

    public function __construct(array $params = array(), array $flags = array()) {
        $this->_init($params, $flags);
        parent::__construct($params, $flags);
    }
    
    public function refreshOptions(array $params = array(), array $flags = array()) {
        if (isset($this->_pointer)) {
            fclose($this->_pointer);
        }
        $this->_pointer = NULL;
        $this->_flags = NULL;
        $this->_params = NULL;
        $this->_init($params, $flags);
    }

    public function log($level, $message, array $context = array()) {
        $errorlevel = '[' . strtoupper($level) . ']';
        $timestamp = '[' . date_create()->format('Y-m-d H:i:s') . ']';
        $context['LEVEL'] = $errorlevel;
        $context['TIMESTAMP'] = $timestamp;
        $trace = debug_backtrace();
        array_shift($trace);
        //filter the global static accessor method, if that is the origin of the call.
        if (!empty($trace) && (isset($trace[0]['class']) && isset($trace[0]['function']) && isset($trace[0]['type'])) && (($trace[0]['class'] == 'oroboros\Oroboros') && ($trace[0]['function'] == 'log') && ($trace[0]['type'] == '::'))) {
            array_shift($trace);
        }
        $last = array_shift($trace);
        $context['ORIGIN'] = NULL;
        if (isset($last)) {
            $details = array(
                'class' => ((isset($last['class'])) ? $last['class'] : NULL),
                'line' => ((isset($last['line'])) ? $last['line'] : NULL),
                'function' => ((isset($last['function'])) ? $last['function'] : NULL),
                'type' => ((isset($last['type'])) ? $last['type'] : NULL),
                'args' => ((isset($last['args'])) ? $last['args'] : NULL),
                'trace' => $trace
            );
            if (isset($details['class'])) {
                //build a class exeption report
                $context['ORIGIN'] = '[::Class: ' . $details['class'] . $details['type'] . $details['function'] . '(); ::]';
            } elseif (!isset($details['class']) && isset($details['file']) && isset($details['function'])) {
                //a named procedural function errored
                $context['ORIGIN'] = '[::Function: ' . $details['function'] . '(); - (Line: ' . $details['line'] . ' of File:' . $details['file'] . ')::]';
            } elseif (isset($details['function']) && !isset($details['file'])) {
                //an anonymous function failed
                $context['ORIGIN'] = '[::Closure::]';
            } else {
                //default origin
                $context['ORIGIN'] = '[::Unknown Origin::]';
            }
        }
        $message = $this->interpolate('[]TIMESTAMP[/][]LEVEL[/][]ORIGIN[/] ' . $message . PHP_EOL, $context);
        $this->_write($message);
    }

    public function __destruct() {
        if (isset($this->_pointer) && is_resource($this->_pointer)) {
            fclose($this->_pointer);
        }
        $this->_pointer = NULL;
        parent::__destruct();
    }
    
    private function _init(array $params = array(), array $flags = array()) {
        if (isset($this->_pointer) && is_resource($this->_pointer)) {
            fclose($this->_pointer);
        }
        $this->_pointer = NULL;
        $setheader = FALSE;
        $this->_params = $params;
        $this->_flags = $flags;
        if (!array_key_exists('log', $params)) {
            throw new \oroboros\src\lib\common\libraries\exception\OroborosException("A log file must be supplied!", self::ERROR_CORE);
        }
        if (in_array("debugMode", $params) && $params['debugMode']) {
//            $flags[] = self::FLAG_TRUNCATE;
        }
        if (count(explode(DIRECTORY_SEPARATOR, $params['log'])) === 1) {
            $params['log'] = OROBOROS_DATA . 'logs' . DIRECTORY_SEPARATOR . $params['log'];
        }
        //check if the log file exists, and/or can be written to
        $filepath = (string) $params['log'];
        $fileIndex = explode(DIRECTORY_SEPARATOR, $filepath);
        $file = array_pop($fileIndex);
        $directory = realpath(implode(DIRECTORY_SEPARATOR, $fileIndex));
        if (!is_writable($directory) && !is_writable($filepath)) {
            throw new \oroboros\src\lib\common\libraries\exception\OroborosException("Supplied logfile: [" . $filepath . "] is not writeable!", self::ERROR_CORE);
        } elseif ((!file_exists($filepath) && is_writable($directory)) 
                || (in_array(self::FLAG_TRUNCATE, $flags))) {
            //create a new log file
            $this->_file_path = $filepath;
            if (file_exists($this->_file_path)) {
                unlink($this->_file_path);
            }
            touch($this->_file_path);
//            $this->_write_mode = (($setheader || in_array(self::FLAG_TRUNCATE, $flags)) ? 'w' : 'a+');
            $this->_write_mode = 'a+';
            $this->_createLogHeading($params, $flags);
        }  else {
            $this->_file_path = $filepath;
            $this->_write_mode = 'a+';
        }
    }

    private function _createLogHeading(array $options = array(), array $flags = array()) {
        $heading = ((isset($options['logHeader']) && $options['logHeader']) ? '[:: ' . $options['logHeader'] . ' ::]' . PHP_EOL : NULL);
        $heading .= ((isset($options['logTimestamp']) && $options['logTimestamp']) ? '[:: Created at: ' .  date_create()->format('Y-m-d H:i:s') . ' ::]' . PHP_EOL   : NULL);
        $heading .= (((isset($options['logHeader']) 
                && $options['logHeader']) && (isset($options['logTimestamp']) && $options['logTimestamp'])) 
                ? '[:: ================ Begin Log ================ ::]' . PHP_EOL . PHP_EOL
                : NULL);
        if (isset($heading)) {
            $this->_write($heading);
        }
    }
    
    private function _write($message) {
        $pointer = fopen($this->_file_path, $this->_write_mode);
            fwrite($pointer, rtrim($message) . PHP_EOL);
            fclose($pointer);
            $pointer = NULL;
    }

}
