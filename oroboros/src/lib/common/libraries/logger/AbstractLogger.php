<?php

namespace oroboros\src\lib\common\libraries\logger;

/*
 * The MIT License
 *
 * Copyright 2016 Brian Dayhoff <brian@mopsyd.me>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * Description of AbstractLogger
 * @author Brian Dayhoff <brian@mopsyd.me>
 */
class AbstractLogger extends \oroboros\src\lib\common\abstracts\OroborosBaseAbstract implements \oroboros\src\lib\common\interfaces\psr3\LoggerInterface, \oroboros\src\lib\common\interfaces\psr3\LogLevel {

    public function __construct(array $params = array(), array $flags = array()) {
        parent::__construct($params, $flags);
    }

    public function __destruct() {
        parent::__destruct();
    }

    public function emergency($message, array $context = array()) {
        return $this->log(self::EMERGENCY, $message, $context);
    }

    public function alert($message, array $context = array()) {
        return $this->log(self::ALERT, $message, $context);
    }

    public function critical($message, array $context = array()) {
        return $this->log(self::CRITICAL, $message, $context);
    }

    public function error($message, array $context = array()) {
        return $this->log(self::ERROR, $message, $context);
    }

    public function warning($message, array $context = array()) {
        return $this->log(self::WARNING, $message, $context);
    }

    public function notice($message, array $context = array()) {
        return $this->log(self::NOTICE, $message, $context);
    }

    public function info($message, array $context = array()) {
        return $this->log(self::INFO, $message, $context);
    }

    public function debug($message, array $context = array()) {
        return $this->log(self::DEBUG, $message, $context);
    }

    public function log($level, $message, array $context = array()) {
        //Null pattern by default
        return TRUE;
    }

    /**
     * Interpolates context values into the message placeholders.
     */
    function interpolate($message, array $context = array()) {
        // build a replacement array with square braces around the context keys
        $replace = array();
        foreach ($context as $key => $val) {
            $replace['[]' . strtoupper($key) . '[/]'] = $val;
        }

        // interpolate replacement values into the message and return
        return strtr($message, $replace);
    }

}
