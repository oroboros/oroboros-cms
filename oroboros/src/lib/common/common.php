<?php

namespace oroboros\src\lib\common;

/*
 * The MIT License
 *
 * Copyright 2016 Brian Dayhoff <brian@mopsyd.me>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */


/**
 * <Oroboros Common Library>
 * A library of common functions 
 * and utilities to make life easier.
 */

/**
 * <Require Classes and Functional Declaration Files Safely>
 * Safely requires a file that should only be included once. If already loaded, 
 * this function will simply return false. If file does not exist, will throw an 
 * exception instead of an error, making it catcheable even if error mode is not 
 * set to exception.
 * @since 0.0.1a
 * @param string $filename The name of the file to load.
 * @return boolean Returns [TRUE] if successfully loaded, [FALSE] if already loaded.
 * @throws \\oroboros\src\lib\common\OroborosException If file does not exist, or is not readable.
 * @errorcode \oroboros\src\lib\common\libraries\exception\OroborosExceptionCodeInterface::ERROR_CORE
 */
function require_safely($filename) {
    //make real system path, to check against relative links
    $file = realpath($filename);
    if (is_readable($file)) {
        if (!in_array($file, get_included_files())) {
            require_once $file;
            return TRUE;
        }
        return TRUE;
    }
    throw new \oroboros\src\lib\common\libraries\exception\OroborosException("[ " . (string) $filename
    . " ] not found or not readable.", interfaces\api\OroborosExceptionCodeInterface::ERROR_CORE);
}

/**
 * <Oroboros Routine Dispatch>
 * Runs a valid procedural routine within the Oroboros core system. 
 * Does not return any output. If the routine is not valid, throws 
 * an OroborosException. If the routine runs, but produces an 
 * exception or error, it will be caught and replaced with an 
 * \OroborosRoutineException, providing the original exception 
 * as the third parameter.
 * @see \oroboros\src\lib\common\libraries\exception\OroborosException
 * @see \oroboros\src\lib\common\libraries\exception\OroborosRuntimeException
 * @param type $routine
 * @param type $safemode
 * @return boolean
 * @throws \oroboros\src\lib\common\libraries\exception\OroborosException
 * @errorcode \oroboros\src\lib\common\api\OroborosExceptionCodeInterface::ERROR_ROUTINE
 * @throws \oroboros\src\lib\common\libraries\exception\OroborosRoutineException
 * @errorcode \oroboros\src\lib\common\api\OroborosExceptionCodeInterface::ERROR_ROUTINE
 * @since 0.0.1a
 */
function oroboros_run_routine($routine, array $params = array(), array $flags = array()) {
    $safemode = ((isset($flags['safemode']) ? $flags['safemode'] : FALSE));
    try {
        $file = realpath(OROBOROS_SRC . 'routines' . DIRECTORY_SEPARATOR
                . strtolower($routine) . '.routine.php');
        $file = ((is_readable($file)) ? $file : FALSE);
        if ($file && $safemode) {
            require_safely($file);
            return (!in_array($file, get_included_files()) ? TRUE : FALSE);
        } elseif ($file && !$safemode) {
            require $file;
        } else {
            throw new \oroboros\src\lib\common\libraries\exception\OroborosException("Routine ["
            . (string) $routine
            . '] could not be loaded!', \oroboros\src\lib\common\api\OroborosExceptionCodeInterface::ERROR_ROUTINE);
            return TRUE;
        }
        return TRUE;
    } catch (\Exception $e) {
        //routine not found
        if (($e instanceof \oroboros\src\lib\common\libraries\exception\OroborosException) && $e->getCode() === \oroboros\src\lib\common\interfaces\api\OroborosExceptionCodeInterface::ERROR_ROUTINE) {
            throw new \oroboros\src\lib\common\libraries\exception\OroborosException("Invalid routine specified: ["
            . (string) $routine . "]", 0, $e);
        } else {
            //routine generated an error or threw an exception
            throw new \oroboros\src\lib\common\libraries\exception\OroborosRoutineException("Error during execution "
            . "of routine [" . $routine . ']' . PHP_EOL . '>'
            . $e->getMessage(), $e->getCode(), $e);
        }
    }
}

/**
 * <Check if Running from Command Line>
 * It is common for scripts to crash if the wrong output headers
 * are sent when a script runs from the command line. This function 
 * will return a simple boolean, allowing you to easily check for this.
 * @return boolean TRUE if cli, FALSE if not cli
 */
function is_cli() {
    return (PHP_SAPI == 'cli' ? TRUE : FALSE);
}

/**
 * <Check if Request is from AJAX>
 * In order for the system to bootstrap the same way for both page 
 * and ajax requests, there needs to be a switch to determine which 
 * kind of request it is, so the correct controller can be loaded 
 * to handle the request. Oroboros uses the standard HTTP ajax header: 
 * X-Requested-With=XMLHttpRequest. 
 * 
 * This function will check if that header was sent to the system, 
 * and load an ajax controller if so.
 * @note This header is not universal despite being mostly standardized, and can also be spoofed. Do no only check for this, use other authentication also.
 * @return boolean TRUE if ajax, FALSE if not ajax
 */
function is_ajax() {
    $headers = request_headers();
    return ($headers && \array_key_exists('X-Requested-With', $headers) && (\strtoupper($headers['X-Requested-With']) === 'XMLHTTPREQUEST'));
}

function request_headers() {
    $response = FALSE;
    if (!is_cli()) {
        if (function_exists("\\apache_request_headers")) {
            $response = \apache_request_headers();
        }
    }
    return $response;
}

/**
 * <Convert Oroboros Core Filepath to Namespaced Classname>
 * This will only work on files within the oroboros directory.
 * Returns [false] if either of: File not found OR outside of oroboros directory.
 * @note This function does not do any explicit checking to insure the actual filepath contains a class, it only produces a valid system namespaced classpath for a class that would be in that file. Inproper usage may cause logic errors.
 * @param string $classFilepath a valid file path to a class file
 * @return boolean|string
 */
function oroboros_classfile_to_classname($classFilepath = FALSE) {
    if (!$classFilepath || (!(is_string($classFilepath) && is_readable($classFilepath)))) {
        return FALSE;
    }
    //get class name
    $classfileIndex = explode(DIRECTORY_SEPARATOR, $classFilepath);
    $classfileName = array_pop($classfileIndex);
    $classname = array_shift(explode('.', $classfileName));

    //get namespace
    while (!empty($classFilepath) && array_shift($classfileIndex) !== "oroboros") {
        
    }
    //If the array is empty, the class is not in the oroboros hierarchy
    if (empty($classfileIndex)) {
        return FALSE;
    }
    return '\\oroboros\\' . implode('\\', $classfileIndex) . '\\' . $classfileName;
}

/**
 * <Localize Path to URL>
 * This will return a URL based on the current 
 * hostname, relative to the Oroboros Core base.
 * @param string $path
 * @return string
 */
function localize_url($path) {
    return str_replace('\\', '/', str_replace(\oroboros\Oroboros::OROBOROS_BASEPATH, OROBOROS_URL . '/oroboros', $path));
}
