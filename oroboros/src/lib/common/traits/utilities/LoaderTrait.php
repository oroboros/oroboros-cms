<?php

namespace oroboros\src\lib\common\traits\utilities;

/*
 * The MIT License
 *
 * Copyright 2016 Brian Dayhoff <brian@mopsyd.me>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * Description of LoaderTrait
 *
 * @author Brian Dayhoff <brian@mopsyd.me>
 */
trait LoaderTrait {

    private $_LoaderTrait_loader_initialized = FALSE;
    private $_LoaderTrait_factory;
    private $_LoaderTrait_allowed_types = array();
    private $_LoaderTrait_loader_types = array(
        "model",
        "view",
        "controller",
        "library",
        "module",
        "template",
        "theme",
        "app"
    );

    protected function _initializeLoader() {
        $this->_LoaderTrait_factory = new \oroboros\src\lib\common\patterns\creational\FactoryFactory();
        $this->_LoaderTrait_factory->initialize();
        $this->_LoaderTrait_loader_initialized = TRUE;
    }

    protected function _setAllowedLoaderTypes(array $types = array()) {
        if ($this->_LoaderTrait_loader_initialized) {
            throw new \oroboros\src\lib\common\libraries\exception\OroborosException(
                    "[" . get_class($this) . "] Allowed loader type declaration must occur before initialization.", 
                    \oroboros\src\lib\common\interfaces\api\OroborosExceptionCodeInterface::ERROR_INITIALIZATION);
        }
        foreach ($types as $type) {
            if (!in_array($type, $this->_LoaderTrait_loader_types)) {
                throw new \oroboros\src\lib\common\libraries\exception\OroborosException(
                        "Invalid loader type [" . (string) $type . "]", 
                        \oroboros\src\lib\common\interfaces\api\OroborosExceptionCodeInterface::ERROR_LOGIC_BAD_PARAMETERS);
            }
        }
        $this->_LoaderTrait_allowed_types = $types;
    }

    protected function _load($type, $subtype, $resource = FALSE, array $params = array(), array $flags = array()) {
        if (!in_array($type, $this->_LoaderTrait_allowed_types)) {
            throw new \oroboros\src\lib\common\libraries\exception\OroborosException(
                    "Loading of type [" . $type . "] is not allowed for class: [" . __CLASS__ . "]", 
                    \oroboros\src\lib\common\interfaces\api\OroborosExceptionCodeInterface::ERROR_LOGIC_BAD_PARAMETERS);
        }
        try {
            $factory = $this->_LoaderTrait_factory->load($type);
            $class = $factory->fetch($subtype, $resource, $params, $flags);
            return $class;
        } catch (\Exception $e) {
            //cast the exception to an expected type
            throw new \oroboros\src\lib\common\libraries\exception\OroborosException(
                    "Error during load sequence for " . (($resource) ? '[' .$subtype . ']::[' . $type . ']::[' . $resource . ']:' : "[" . $type . "]::[" . $subtype . "]:") . " " . $e->getMessage(), 
                    \oroboros\src\lib\common\interfaces\api\OroborosExceptionCodeInterface::ERROR_INITIALIZATION, $e);
        }
    }

}
