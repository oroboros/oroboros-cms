<?php

/*
 * The MIT License
 *
 * Copyright 2016 Brian Dayhoff <brian@mopsyd.me>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\src\lib\common\traits\utilities;

/**
 * <HtmlBuilder Trait>
 * Assists in building complicated html markup dynamically.
 * @author Brian Dayhoff <brian@mopsyd.me>
 */
trait HtmlBuilder {

    private $_HtmlBuilder_void_elements = array ('area', 'base', 'br', 'col', 'embed', 'hr', 'img', 'input', 'keygen', 'link', 'meta', 'param', 'source', 'track', 'wbr');
    
    protected function _tag($tagname, array $params = array(), array $flags = array()) {
        $markup = '<' . trim(strtolower($tagname));
        $content = NULL;
        $content = ((isset($params['content'])) ? $params['content'] : NULL);
        if (isset($params['content'])) {
            $content = $params['content'];
            unset($params['content']);
        }
        foreach ($params as $key => $value) {
            $markup .= ' ' . $key . '="' . $value . '"';
        }
        $markup .= '>';
        if (in_array(self::FLAG_SHORT_TAG, $flags) || in_array(trim(strtolower($tagname)), $this->_HtmlBuilder_void_elements) ) {
            return $markup . PHP_EOL;
        }
        $markup .= $content . '</' . trim(strtolower($tagname)) . '>' . ((!in_array(self::FLAG_INLINE_TAG, $flags)) ? PHP_EOL : NULL);
        return $markup;
    }
    
    protected function _markdown($source) {
        $parser = new \Parsedown();
        $markdown = ((file_exists($source)) ? file_get_contents($source) : $source);
        return $parser->text($markdown);
    }

}
