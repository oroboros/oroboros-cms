<?php

/*
 * The MIT License
 *
 * Copyright 2016 Brian Dayhoff <brian@mopsyd.me>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * <Oroboros Installer Routine>
 * This command set will run the Oroboros Core installer. This should only ever 
 * be called one time on any fresh installation.
 */
call_user_func(function() {
    try {
        $installer_definitions = OROBOROS_SYSTEM
                . 'index' . DIRECTORY_SEPARATOR
                . 'install' . DIRECTORY_SEPARATOR
                . 'definitions.php';
        oroboros\src\lib\common\require_safely($installer_definitions);
        $bootstrap = new \oroboros\src\system\index\install\InstallationBootstrap();
        if (\oroboros\src\lib\common\is_ajax()) {
            //defer the request to the ajax controller
            $params = array();
            $flags = array($bootstrap::FLAG_MODE_AJAX);
        } elseif (\oroboros\src\lib\common\is_cli()) {
            //perform the command line execution task
            $params = array();
            $flags = array($bootstrap::FLAG_MODE_CLI);
        } else {
            //Load the HTTP installer interface
            $params = array();
            $flags = array();
        }
        $bootstrap->initialize($params, $flags);
        $bootstrap->launch();
        exit();
    } catch (\oroboros\src\lib\common\libraries\exception\OroborosException $e) {
        //Uncaught error with the installation module
        \oroboros\Oroboros::log(\oroboros\Oroboros::EMERGENCY, "Core installation failed! " . PHP_EOL . '--->' . $e->getMessage() . ((NULL !== $e->getPrevious()) ? $e->getPrevious()->getMessage() : NULL));
        throw $e;
    }
    exit();
});