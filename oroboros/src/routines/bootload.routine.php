<?php

/*
 * The MIT License
 *
 * Copyright 2016 Brian Dayhoff <brian@mopsyd.me>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * <Oroboros Bootloader>
 * This routine represents loads all of the resources required for startup, but 
 * does not execute or initialize anything. This is separated from the front 
 * controller in order to enable unit testing.
 */
call_user_func(function() {
    try {
        if (!defined("OROBOROS")) {
            exit("This file requires initialization before running");
        }
        /**
         * Basic system navigation definitions, 
         * mostly used for pre-bootstrap operations, 
         * also provided for convenience.
         */
        require_once OROBOROS
                . "config" . DIRECTORY_SEPARATOR
                . "definitions.php";
        require_once OROBOROS_LIB
                . 'common' . DIRECTORY_SEPARATOR
                . 'common.php';
        /**
         * Check if PHP needs to be patched for the system to work.
         */
        if (!version_compare(PHP_VERSION, '5.5.0')) {
            //compatibility patches for older php versions
            require_once OROBOROS_LIB . 'common' . DIRECTORY_SEPARATOR . "patch.php";
        }
        //require the autoloader safely in case it 
        //has already been included for some reason.
        \oroboros\src\lib\common\require_safely(
                OROBOROS_LIB
                . 'common' . DIRECTORY_SEPARATOR
                . 'libraries' . DIRECTORY_SEPARATOR
                . 'psr4' . DIRECTORY_SEPARATOR
                . 'Autoloader.php'
        );
        $autoloader = new \oroboros\src\lib\common\libraries\psr4\Autoloader();
        //Add the base namespace
        $autoloader->addNamespace('oroboros', OROBOROS);
        //Add the Psr namespaces
        $autoloader->addNamespace('Psr\\Log', OROBOROS_DEPENDENCIES
                . 'vendor' . DIRECTORY_SEPARATOR
                . 'psr' . DIRECTORY_SEPARATOR
                . 'log' . DIRECTORY_SEPARATOR
                . 'Psr' . DIRECTORY_SEPARATOR
                . 'Log');
        $autoloader->addNamespace('Psr\\Cache', OROBOROS_DEPENDENCIES
                . 'vendor' . DIRECTORY_SEPARATOR
                . 'psr' . DIRECTORY_SEPARATOR
                . 'cache' . DIRECTORY_SEPARATOR
                . 'src');
        $autoloader->addNamespace('Fig\\Cache', OROBOROS_DEPENDENCIES
                . 'vendor' . DIRECTORY_SEPARATOR
                . 'psr' . DIRECTORY_SEPARATOR
                . 'cache-util' . DIRECTORY_SEPARATOR
                . 'src');
        $autoloader->addNamespace('Psr\\Http\\Message', OROBOROS_DEPENDENCIES
                . 'vendor' . DIRECTORY_SEPARATOR
                . 'psr' . DIRECTORY_SEPARATOR
                . 'http-message' . DIRECTORY_SEPARATOR
                . 'src');
        //Add Parsedown (not namespaced, grr...)
        require_once (OROBOROS_DEPENDENCIES
                . 'vendor' . DIRECTORY_SEPARATOR
                . 'erusev' . DIRECTORY_SEPARATOR
                . 'parsedown' . DIRECTORY_SEPARATOR
                . 'Parsedown.php');
        $autoloader->register();
        \oroboros\Oroboros::setAutoloader($autoloader);
        //Includes the Composer autoloader, if it exists
        if (file_exists("oroboros/lib/dependencies/vendor/autoload.php") && is_readable("oroboros/lib/dependencies/vendor/autoload.php")) {
            \oroboros\src\lib\common\require_safely("oroboros/lib/dependencies/vendor/autoload.php");
        }
    } catch (\Exception $e) {
        throw new \oroboros\src\lib\common\libraries\exception\OroborosException("[CRITICAL][***FATAL ERROR***] Uncaught exception during bootload process: "
        . $e->getMessage(), $e->getCode(), $e);
    }
});
