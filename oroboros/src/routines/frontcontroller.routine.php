<?php

/*
 * The MIT License
 *
 * Copyright 2016 Brian Dayhoff <brian@mopsyd.me>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * <Oroboros Front Controller>
 * This routine represents the standard front controller. It loads the required 
 * resources needed for the actual bootstrap class to operate and checks 
 * for the installation fingerprint file. If the fingerprint file is 
 * missing, it will instead defer to the installer.
 */
call_user_func(function() {
    try {
        if (!defined("OROBOROS")) {
            exit("This file requires initialization before running");
        }
        require_once OROBOROS . 'src' . DIRECTORY_SEPARATOR . 'routines'
                . DIRECTORY_SEPARATOR . 'bootload.routine.php';
        //Initialize Oroboros Core - standard initialization
        //using the standard system settings to initialize
        \oroboros\Oroboros::init();
        //If installed, run the bootstrap. If not installed, run the installer.
        if (file_exists(OROBOROS_DATA . 'installation.fingerprint') && is_readable(OROBOROS_DATA . 'installation.fingerprint')) {
            //Run the standard bootstrap routine
            \oroboros\src\lib\common\oroboros_run_routine('bootstrap');
            exit();
        } else {
            //System is not configured, run the installer routine.
            \oroboros\src\lib\common\oroboros_run_routine('install');
            exit();
        }
    } catch (\Exception $e) {
        //...well this is bad. 
        try {
            //Log the error and throw a fatal error exception.
            $r = new ReflectionClass($e);
            $constantNames = array_flip($r->getConstants());
            $message = '[:: FATAL ERROR ::] Uncaught exception: [' . get_class($e) . ']' . PHP_EOL
                    . '--->Type: ' . ((isset($constantNames[$e->getCode()])) ?$constantNames[$e->getCode()] . PHP_EOL : 'Unknown Type' . PHP_EOL)
                    . '--->Error Code: ' . $e->getCode() . PHP_EOL
                    . '--->With Message: ' . $e->getMessage() . PHP_EOL
                    . '--->[:: BACKTRACE ::]' . PHP_EOL . $e->getTraceAsString() . PHP_EOL
                    . '--->[:: Previous ::]' . PHP_EOL 
                    . '--->' . $e->getPrevious();
            \oroboros\Oroboros::log(\oroboros\Oroboros::EMERGENCY, $message);
            \dev\Utils::crash($message);
        } catch (\Exception $e) {
            //...well this is really bad. Logging failed. Throw fatal error exception.
            throw new \oroboros\src\lib\common\libraries\exception\OroborosException("[CRITICAL][***FATAL ERROR***] Logging failed for uncaught exception: "
            . $e->getMessage(), $e->getCode(), $e);
        }
    }
});

