<?php

namespace oroboros\src\system\library\views\install;

/*
 * The MIT License
 *
 * Copyright 2016 Brian Dayhoff <brian@mopsyd.me>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * <Oroboros Installation View>
 *
 * @author Brian Dayhoff <brian@mopsyd.me>
 */
final class InstallationView extends \oroboros\src\lib\common\abstracts\views\AbstractHtmlView {

    const DEFAULT_TEMPLATE = 'install\\InstallTemplate';
    const DEFAULT_TEMPLATE_SOURCE = 'system';

    private $_install_scripts = array();
    private $_install_css = array();
    private $_install_content = array();
    private $_metadata = array();

    public function __construct(array $params = array(), array $flags = array()) {
        parent::__construct($params, $flags);
    }

    protected function _setPage($page) {
        $params = array(
            "pagename" => $page['step_current'],
        );
        $this->_queueTemplateData($params);
    }
    
    protected function _setMetadata(array $metadata) {
        $this->_queueTemplateData(array("metadata" => $metadata));
    }
    
    protected function _setTitle($title) {
        $params = array(
            "title" => $title,
        );
        $this->_queueTemplateData($params);
    }

    protected function _setSchema($schema) {
        $params = array(
            "heading" => $schema->title,
        );
        if (isset($schema->css)) {
            $this->_buildCss($schema->css);
            $params['css'] = $this->_install_css;
        }
        if (isset($schema->scripts)) {
            $this->_buildScripts($schema->scripts);
            $params['scripts'] = $this->_install_scripts;
        }
        if (isset($schema->message)) {
            $this->_install_content = $this->_buildContentArray(array("message" => $schema->message), $this->_install_content);
            $params['content'] = $this->_install_content;
        }
        $this->_queueTemplateData($params);
    }

    protected function _setResources($resources) {
        $params = array();
        if (isset($resources->always) && isset($resources->always->css)) {
            $this->_buildCss($resources->always->css);
            $params['css'] = $this->_install_css;
        }
        if (isset($resources->always) && isset($resources->always->scripts)) {
            $this->_buildScripts($resources->always->scripts);
            $params['scripts'] = $this->_install_scripts;
        }
        $this->_queueTemplateData($params);
    }

    private function _buildCss($css) {
        $result = array();
        if (isset($css->local)) {
            $result = array_merge($result, get_object_vars($this->_buildLocal('css', $css->local)));
        }
        if (isset($css->cdn)) {
            $result = array_merge($result, get_object_vars($this->_buildCdn('css', $css->cdn)));
        }
        $this->_install_css = $this->_prioritizeLoadOrder($result, $this->_install_css);
    }
    
    private function _buildScripts($js) {
        $result = array();
        if (isset($js->local)) {
            $result = array_merge($result, get_object_vars($this->_buildLocal('scripts', $js->local)));
        }
        if (isset($css->cdn)) {
            $result = array_merge($result, get_object_vars($this->_buildCdn('scripts', $js->cdn)));
        }
        $this->_install_scripts = $this->_prioritizeLoadOrder($result, $this->_install_scripts);
    }

    private function _buildLocal($type, $resource) {
        foreach ($resource as $name => $details) {
            $linktype = ((isset($details->href)) ? 'href' : 'src');
            $resource->{$name}->{$linktype} = $this->_localizeInstallationResourceUrl($type, $details->{$linktype});
        }
        return $resource;
    }

    private function _buildCdn($type, $resource) {
        foreach ($resource as $name => $details) {
            $linktype = ((isset($details->href)) ? 'href' : 'src');
            $resource->{$name}->{$linktype} = $details->{$linktype};
        }
        return $resource;
    }
    
    private function _buildContentArray($content, $old) {
        foreach ($content as $key => $value) {
            $old[$key] = $value;
        }
        return $old;
    }

    private function _prioritizeLoadOrder($resource, array $order) {
        foreach ($resource as $key => $value) {
            $order[$value->priority][$key] = $value;
        }
        ksort($order);
        return $order;
    }

    private function _localizeInstallationResourceUrl($type, $resource) {
        $path = OROBOROS_SYSTEM
                . 'resources' . DIRECTORY_SEPARATOR
                . $type . DIRECTORY_SEPARATOR
                . $resource;
        return \oroboros\src\lib\common\localize_url($path);
    }

}
