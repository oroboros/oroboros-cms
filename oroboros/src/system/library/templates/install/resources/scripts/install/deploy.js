oroboros.env = {
    taskProgress: null,
    datatable: null,
    taskset: null,
    evaluate: function () {
        oroboros.installer.ajax("envcheck", {}, function (data, status, xhr) {
            console.log(data.responseText);
        });
    },
    tabulate: function () {
        oroboros.installer.ajax("envtable", {}, function (data, status, xhr) {
            console.info("Tabulation results:");
            if (data) {
                console.info(data.responseText);
                oroboros_debug_log(data.responseText);
                var response = JSON.parse(data.responseText);
            }
            console.log(response.data);
            var taskcount = 0;
            jQuery.each(response.data, function (key, value) {
                console.log(key);
                console.groupCollapsed(key);
                console.info(value.meta.category);
                console.groupEnd();
                jQuery.each(value.tasks, function (subkey, task) {
                    var markup = '<tr id="' + key + '-' + subkey + '" class="">\n' +
                            '<th scope="row" class="category">' + value.meta.category + '</th>\n' +
                            '<th scope="row" class="taskname">' + task.title + '</th>\n' +
                            '<td class="status">\n' +
                            '<span class="text-muted glyphicon glyphicon-refresh glyphicon-spin"></span></td>\n' +
                            '<td class="diagnosis"><p class="text-muted">checking...</p></td>\n' +
                            '<td class="notes">' + 
                            '<p class="pending text-info"><span class="glyphicon glyphicon-time"></span> Awaiting assessment...</p>' + 
                            '<p class="fail text-danger hidden"><span class="glyphicon glyphicon-remove"></span> ' + task.message.fail + '</p>' + 
                            '<p class="success text-success hidden"><span class="glyphicon glyphicon-ok"></span> ' + task.message.success + '</p>' + 
                            '</td>\n' +
                            '</tr>';
                    jQuery('table#environment-results').children('tbody').append(markup);
                    taskcount++;
                });
            });
            this.taskProgress = new oroboros.ui.progressbar('#environment-progress', taskcount);
            this.taskProgress.complete(function () {
                jQuery(this.taskProgress.id).removeClass('progress-bar-striped');
                jQuery(this.taskProgress.id).removeClass('active');
            });
            //Tabulation complete, safe to add dataTables styles (must be done in AJAX callback)
            this.datatable = jQuery('#environment-results').dataTable({
                initComplete: function(settings) {
                    jQuery('#environment-results_length').append('<span class="text-primary"> (<span class="value-current">0</span> / <span class="value-max">' + taskcount + '</span> completed)</span>');
                }
            });
        });
    },
    check: function (key) {
        oroboros.installer.ajax("check", {key: key}, function (data, status, xhr) {
            console.log(data.responseText);
        });
    },
    progress: function ($value) {
        this.taskProgress.increase($value);
    }
};

function oroboros_debug_log($data) {
    jQuery('#debug_log').append('<span class="debug-log-entry">'+$data+'</span>');
}

jQuery(document).ready(function () {
    oroboros.env.tabulate();
    oroboros.env.evaluate();
});