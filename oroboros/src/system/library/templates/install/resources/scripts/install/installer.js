oroboros.initialize = function () {
    this.metadata = oroboros_core_metadata() || {};
};
oroboros.ui = {
    progressbar: function (id, steps) {
        var ProgressBarController = function (id, steps) {
            this.id = id;
            this.increment = {
                pointer: 0,
                min: 0,
                max: steps,
                increment: (100 / steps)
            };
            this.update();
            this.data = {
            };
            return ((this instanceof window.constructor)
                    ? new ProgresBarController(id, steps)
                    : this);
        }
        ProgressBarController.prototype = {
            element: function () {
                return this.id;
            },
            update: function () {
                jQuery(this.id).attr("aria-valuenow", this.increment.pointer);
                jQuery(this.id).attr("aria-valuemin", this.increment.min);
                jQuery(this.id).attr("aria-valuemax", this.increment.max);
            },
            increase: function (steps) {
                var diff = Math.round(this.increment.pointer
                        + (this.increment.increment * steps));
                if (diff >= this.increment.max) {
                    this.increment.pointer = this.increment.max;
                } else {
                    this.increment.pointer = (diff);
                }
                this.update();
                this.animate();
                if (this.increment_callback && jQuery.isFunction(this.increment_callback)) {
                    this.increment_callback(this.element(), this.increment, this.data);
                }
                if (this.increment.pointer >= this.increment.max) {
                    if (this.max_callback && jQuery.isFunction(this.max_callback)) {
                        this.max_callback(this.element(), this.increment, this.data);
                    }
                }
            },
            decrease: function (steps) {
                var diff = Math.round(this.increment.pointer - (this.increment.increment * steps));
                if (diff <= this.increment.min) {
                    this.increment.pointer = this.increment.min;
                } else {
                    this.increment.pointer = (diff);
                }
                this.update();
                this.animate();
                if (this.increment_callback && jQuery.isFunction(this.increment_callback)) {
                    this.increment_callback(this.element(), this.increment, this.data);
                }
                if (this.increment.pointer <= this.increment.min) {
                    if (this.min_callback && jQuery.isFunction(this.min_callback)) {
                        this.min_callback(this.element(), this.increment, this.data);
                    }
                }
            },
            animate: function () {
                var val = Math.round(((this.increment.pointer / this.increment.max) * 100));
//                    jQuery(this.id).animate({
//                        width: (val/100)*jQuery(this.id).width(),
//                    });
                jQuery(this.id).width(val + "%");
            },
            complete: function (callback) {
                if (!(jQuery.isFunction(callback))) {
                    throw new Error("Invalid function passed at ");
                }
                this.max_callback = callback;
            },
            empty: function (callback) {
                if (!(jQuery.isFunction(callback))) {
                    throw new Error("");
                }
                this.min_callback = callback;
            },
            change: function (callback) {
                if (!(jQuery.isFunction(callback))) {
                    throw new Error("");
                }
                this.increment_callback = callback;
            }
        }
        return new ProgressBarController(id, steps);
    }
};
window.oroboros.installer = (function () {
    var OroborosInstaller = function (s, a, f) {
        return ((this instanceof window.constructor)
                ? new OroborosInstaller(s, a, f)
                : this);
    };
    OroborosInstaller.prototype.ajax = function (command, args, callback) {
        var data = JSON.stringify({"command": command, "args": args});
        jQuery.ajax({
            url: window.oroboros.metadata.app.ajax.url,
            type: "post",
            data: {data: data},
            complete: callback
        });
    };
    OroborosInstaller.prototype.initialize = (function (parent) {
        var OroborosInstallerConstructor = function () {
            if (!(this instanceof window.constructor)) {
                this.initialize();
                return this;
            } else {
                return new OroborosInstallerConstructor(parent);
            }
        }
        OroborosInstallerConstructor.prototype.metaData = function () {
            this.metadata = parent.metadata || oroboros.metadata;
            console.info(this.metadata);
        }
        OroborosInstallerConstructor.prototype.initializeState = function () {
            this.state = new parent.app.pattern.State({
                initialize: function () {
                },
                update: function () {
                },
                render: function () {
                },
            });
        };
        OroborosInstallerConstructor.prototype.initialize = function () {
            this.metaData();
            this.initializeState();
            this.initialized = true;
            return this;
        };

        return ((this instanceof window.constructor) ? new OroborosInstallerConstructor : this);
    })(oroboros);
    return new OroborosInstaller();
})();

//wizard adapter
oroboros.ui.wizard = function (selector, params) {
    var OroborosWizardAdapter = function (selector, params) {
        if (!(this instanceof window.constructor)) {
            this.selector = selector;
            this.params = params;
            return this;
        } else {
            return new OroborosWizardAdapter(selector, params);
        }
    }
    OroborosWizardAdapter.prototype.constructor = OroborosWizardAdapter;
    OroborosWizardAdapter.prototype.initialize = function () {
        this.wizard = jQuery(this.selector).bootstrapWizard(this.params);
    };
    OroborosWizardAdapter.prototype.unload = function () {
    };
    OroborosWizardAdapter.prototype.show = function (id) {
        jQuery(this.selector).bootstrapWizard('display', jQuery(id).val());
        jQuery(this.selector).find(id).removeClass('hidden');
    };
    OroborosWizardAdapter.prototype.hide = function (id) {
        jQuery(this.selector).bootstrapWizard('hide', jQuery(id).val());
        jQuery(this.selector).find(id).addClass('hidden');
    };
    OroborosWizardAdapter.prototype.enable = function (id) {
        jQuery(this.selector).bootstrapWizard('enable', jQuery(id).val());
        jQuery(this.selector).find(id).removeClass('disabled');
    };
    OroborosWizardAdapter.prototype.disable = function (id) {
        jQuery(this.selector).bootstrapWizard('disable', jQuery(id).val());
        jQuery(this.selector).find(id).addClass('disabled');
    };
    OroborosWizardAdapter.prototype.next = function () {

    };
    OroborosWizardAdapter.prototype.previous = function () {

    };
    OroborosWizardAdapter.prototype.set = function (id) {
        jQuery(this.selector).find("a[href*='" + id + "']").trigger('click');
    };
    OroborosWizardAdapter.prototype.add = function (id, details) {

    };
    OroborosWizardAdapter.prototype.remove = function (id) {

    };
    OroborosWizardAdapter.prototype.tabClick = function (callback) {
        jQuery(this.selector).bootstrapWizard({onTabClick: callback});
    };

    return new OroborosWizardAdapter(selector, params);
}

jQuery(document).ready(function () {
    jQuery.noConflict();
    oroboros.initialize();
    //button disable assist
    jQuery('.btn-group .btn.disabled').click(function (event) {
        event.stopPropagation();
        event.preventDefault();
        return false;
    });
    //register unload function
    jQuery(window).on('beforeunload', function () {
        window.oroboros.exit();
    });
    //extension scripts
    //tooltips
    jQuery('[data-toggle="tooltip"]').tooltip();
});