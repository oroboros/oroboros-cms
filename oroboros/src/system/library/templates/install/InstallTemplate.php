<?php
namespace oroboros\src\system\library\templates\install;
/*
 * The MIT License
 *
 * Copyright 2016 Brian Dayhoff <brian@mopsyd.me>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * Description of InstallTemplate
 *
 * @author Brian Dayhoff <brian@mopsyd.me>
 */
final class InstallTemplate extends \oroboros\src\lib\common\abstracts\libraries\template\TemplateAbstract 
{
    
    private $_data = array();
    
    public function __construct(array $params = array(), array $flags = array()) {
        parent::__construct($params, $flags);
    }
    
    public function initialize(array $params = array(), array $flags = array()) {
        parent::initialize($params, $flags);
        $this->_registerMap(realpath(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'schema.json');
        $this->_registerData($params);
    }
    
    public function queue(array $params = array(), array $flags = array()) {
        parent::queue($params, $flags);
    }
    
    public function setData($key, $data) {
        $this->_data[$key] = $data;
    }
    
    public function data($key) {
        return (isset($this->_data[$key]) ? $this->_data[$key] : NULL);
    }
}
