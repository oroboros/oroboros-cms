<?php

namespace oroboros\src\system\library\models\install;

/*
 * The MIT License
 *
 * Copyright 2016 Brian Dayhoff <brian@mopsyd.me>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * Description of InstallerModel
 *
 * @author Brian Dayhoff <brian@mopsyd.me>
 */
class InstallationModel extends \oroboros\src\lib\common\abstracts\models\AbstractModel {

    private $_schema;
    private $_router;
    private $_steps = array();
    private $_stepTypes = array(
        "message",
        "form",
        "error"
    );
    private $_default_step = "welcome";
    private $_default_stepType = "message";
    private $_current_step = false;
    private $_current_stepType = false;
    private $_complete = false;
    private $_required = true;
    private $_title;
    private $_message;
    private $_data;

    public function initialize(array $params = array(), array $flags = array()) {
        try {
            if (!isset($params['schema'])) {
                throw new \oroboros\src\lib\common\libraries\exception\OroborosException("Missing initialization parameter: [schema]", self::ERROR_MODEL);
            }
            $this->_loadInstallSchema($params['schema']);
            if (!isset($params['router'])) {
                throw new \oroboros\src\lib\common\libraries\exception\OroborosException("Missing initialization parameter: [router]", self::ERROR_MODEL);
            }
            $this->_router = $params['router'];
            $this->_logger = ((isset($params['logger'])) ? $params['logger'] : \oroboros\Oroboros::getLoggerObject());
            $this->_checkInstallationProgress();
            $this->_data = $this->_buildStepData();
            parent::initialize($params, $flags);
        } catch (\Exception $e) {
            throw new \oroboros\src\lib\common\libraries\exception\OroborosException('Model initialization failure!', self::ERROR_MODEL, $e);
        }
    }

    public function step() {
        return (($this->_current_step) ? $this->_current_step : $this->_default_step);
    }

    public function next() {
        $current = array_search($this->step(), $this->_steps);
        if ((count($this->_steps) - 1) == $current) {
            return FALSE;
        }
        return $this->_steps[$current + 1];
    }

    public function previous() {
        $current = array_search($this->step(), $this->_steps);
        if ($current == 0) {
            return FALSE;
        }
        return $this->_steps[$current - 1];
    }

    public function steps($include_meta = FALSE) {
        if (!$include_meta) {
            return $this->_steps;
        }
        $steps = $this->_steps;
        $result = array();
        $step_completion = $this->_getInstallationFileData();
        foreach ($steps as $key => $step) {
            $result[$step] = array(
                "required" => $this->_schema->{$step}->required,
                "render" => $this->_schema->{$step}->render,
                "title" => $this->_schema->{$step}->title,
                "icon" => $this->_schema->{$step}->icon,
                "complete" => $step_completion->{$step}->complete,
                "previous" => ((isset($steps[$key - 1])) ? $steps[$key - 1] : FALSE),
                "next" => ((isset($steps[$key + 1])) ? $steps[$key + 1] : FALSE),
            );
        }
//        \dev\Utils::crash($result);
        return $result;
    }

    public function setStep($step) {
        $this->_updateStep($step);
    }

    public function process() {
        $this->_process();
    }

    public function stepType() {
        return (($this->_current_stepType) ? $this->_current_stepType : $this->_default_stepType);
    }

    public function stepTitle() {
        return $this->_title;
    }

    public function stepMessage() {
        return $this->_message;
    }

    public function stepData($step = NULL) {
        $step_data = ((isset($step)) ? ((in_array($step, $this->_steps)) ? $this->_buildStepData($step) : FALSE) : $this->_data);
        return $step_data;
    }
    
    public function stepProgressData($step = NULL) {
        return ((isset($step)) ? $this->_getInstallationFileSectionData($step) : $this->_getInstallationFileData());
    }

    public function updateStepData($step, $status = FALSE, $data = array()) {
        $this->_writeToProgressFile($step, $status, (empty($data) ? NULL : $data));
    }

    private function _loadInstallSchema($schema) {
        try {
            $this->_schema = $schema;
            $this->_steps = array_keys((array) $this->_schema);
        } catch (\Exception $e) {
            throw new \oroboros\src\lib\common\libraries\exception\OroborosException("Invalid installation schema detected.", self::ERROR_MODEL, $e);
        }
    }

    private function _updateStep($step) {
        if (!in_array($step, $this->_steps)) {
            throw new \oroboros\src\lib\common\libraries\exception\OroborosException("Unknown installation step ["
            . (string) $step . "]!", self::ERROR_MODEL);
        }
        $this->_current_step = $step;
        $this->_data = $this->_buildStepData($step);
    }

    /**
     * 
     * @param type $step
     */
    private function _buildStepData($step = NULL) {
        $evaluate_step = ((isset($step)) ? $step : $this->step());
        $data = [];
        $this->_title = ((isset($this->_schema->{$evaluate_step}->title)) ? $this->_schema->{$evaluate_step}->title : false);
        $this->_message = ((isset($this->_schema->{$evaluate_step}->message)) ? $this->_schema->{$evaluate_step}->message : false);
        $key = array_search($evaluate_step, $this->_steps);
        $data['step_current'] = $evaluate_step;
        $data['progress'] = $this->_getInstallationFileSectionData($evaluate_step);
        //define back/forward routing markers
        $data['step_previous'] = $this->previous();
        $data['step_next'] = $this->next();
        //form data
        $data['form'] = ((isset($this->_schema->{$evaluate_step}->form)) ? $this->_schema->{$evaluate_step}->form : FALSE);
        if (isset($this->_schema->{$evaluate_step}->errors)) {
            $data['errors'] = $this->_schema->{$evaluate_step}->errors;
        }
        if (isset($this->_schema->{$evaluate_step}->redirect)) {
            $data['redirect'] = $this->_schema->{$evaluate_step}->redirect;
            $data['redirect_step'] = (
                    (isset($this->_schema->{$evaluate_step}->stepTo)) ? $this->_schema->{$evaluate_step}->stepTo : FALSE );
            $data['redirect_location'] = (
                    (isset($this->_schema->{$evaluate_step}->location)) ? $this->_schema->{$evaluate_step}->location : FALSE);
        }
        $data['scripts'] = $this->_checkStepScripts($evaluate_step);
        $data['css'] = $this->_checkStepCss($evaluate_step);
        $data['validate'] = ((isset($this->_schema->{$evaluate_step}->validate)) ? $this->_schema->{$evaluate_step}->validate : FALSE);

        return $data;
    }

    private function _checkStepPageScripts($type, $path) {
        if (isset($this->_schema->{$evaluate_step}->{$type})) {
            return FALSE;
        }
        $result = array();
        if (isset($this->_schema->{$evaluate_step}->scripts->local)) {
            $path = $this->_router->client() . $path . '/';
            foreach ($this->_schema->{$evaluate_step}->{$type}->local as $script => $file) {
                $result[] = $path . $file;
            }
        } elseif (isset($this->_schema->{$evaluate_step}->{$type}->remote)) {
            foreach ($this->_schema->{$evaluate_step}->{$type}->remote as $script => $file) {
                $result[] = $file;
            }
        }
        return $result;
    }

    private function _checkStepCss($evaluate_step) {
        return (isset($this->_schema->{$evaluate_step}->css) ? get_object_vars($this->_schema->{$evaluate_step}->css) : array());
    }

    private function _checkStepScripts($evaluate_step) {
        return (isset($this->_schema->{$evaluate_step}->scripts) ? get_object_vars($this->_schema->{$evaluate_step}->scripts) : array());
    }
    
    private function _updateFromProgressFile() {
        $step_complete = FALSE;
        $raw = $this->_getInstallationFileData();
        $current = $raw->{$this->step()};
        $this->_complete = $current->complete;
        $this->_data = $current->data;
        $this->_updateStep($this->step());
    }

    /**
     * Writes updated data to the installation temporary progress file.
     * @param string $key The index of the dataset in the file.
     * @param boolean $status whether or not the step has been completed.
     * @param mixed $newdata The data to write into the file.
     * @throws \OroborosInstallerException
     * @return void
     */
    private function _writeToProgressFile($key, $status = FALSE, $newdata = false) {
        $data = $this->_getInstallationFileData();
        if (!isset($data->{$key})) {
            throw new \oroboros\src\lib\common\libraries\exception\OroborosException(
            "Could not write progress to an invalid installation step!", self::ERROR_INSTALLATION);
        }
        $result = $data->{$key};
        $result->complete = $status;
        $result->data = $newdata;
        $data->{$key} = $result;
        $pointer = fopen(OROBOROS_TMP . 'installation.progress.json', 'w+');
        fwrite($pointer, json_encode($data, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES  | JSON_UNESCAPED_UNICODE));
        fclose($pointer);
    }
    
    private function _checkInstallationProgress() {
        $file = OROBOROS_TMP . 'installation.progress.json';
        if (is_readable($file)) {
            //file exists, load configuration progress from file
            $this->_updateFromProgressFile();
        } else {
            //file does not exist, create file for new installation progress
            $this->_createInstallationFile();
            $this->_updateFromProgressFile();
        }
    }

    private function _getInstallationFileData() {
        $file = OROBOROS_TMP . 'installation.progress.json';
        try {
            $data = json_decode(file_get_contents($file));
            return $data;
        } catch (\Exception $e) {
            throw new \oroboros\src\lib\common\libraries\exception\OroborosException("Could not update the installation file!"
            . PHP_EOL . '>reason: ' . $e->getMessage(), self::ERROR_FILESYSTEM, $e);
        }
    }

    private function _getInstallationFileSectionData($key) {
        $data = $this->_getInstallationFileData();
        return ((isset($data->{$key})) ? $data->{$key} : FALSE);
    }

    /**
     * Creates the installation temporary data 
     * file if it does not already exist.
     * @throws \oroboros\src\lib\common\libraries\exception\OroborosException
     */
    private function _createInstallationFile() {
        try {
            $progress = array();
            foreach ($this->_steps as $index => $step) {
                $progress[$step] = array("complete" => false, "data" => null);
            }
            $file = OROBOROS_TMP . 'installation.progress.json';
            touch($file);
            $pointer = fopen($file, "a+");
            fwrite($pointer, json_encode($progress, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES));
            fclose($pointer);
        } catch (\Exception $e) {
            throw new \oroboros\src\lib\common\libraries\exception\OroborosException("Unable to create installation.progress.json!", self::ERROR_FILESYSTEM, $e);
        }
    }

}
