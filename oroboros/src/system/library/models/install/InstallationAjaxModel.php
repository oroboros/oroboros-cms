<?php

namespace oroboros\src\system\library\models\install;

/*
 * The MIT License
 *
 * Copyright 2016 Brian Dayhoff <brian@mopsyd.me>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * Description of InstallationAjaxModel
 *
 * @author Brian Dayhoff <brian@mopsyd.me>
 */
class InstallationAjaxModel extends \oroboros\src\lib\common\abstracts\models\AbstractModel {

    const WORKER_NAMESPACE = '\\oroboros\\src\\system\\index\\install\\workers\\';
    private $_schema;
    private $_errors;
    private $_router;
    private $_conditions;
    private $_worker = NULL;
    private $_command = NULL;
    private $_arguments = NULL;

    public function initialize(array $params = array(), array $flags = array()) {
        parent::initialize($params, $flags);
    }

    protected function _setSchema($schema) {
        $this->_schema = $schema;
    }

    protected function _setErrors($errors) {
        $this->_errors = $errors;
    }

    protected function _setConditions($conditions) {
        $this->_conditions = $conditions;
    }

    protected function _setRouter($router) {
        $this->_router = $router;
    }

    public function checkCommand($command) {
        return (method_exists($this, '_' . strtolower($command)));
    }

    public function command($command, array $args = array()) {
        $this->_command = $command;
        $this->_arguments = $args;
        if ($this->checkCommand($command)) {
            try {
                $cmd = $this->_resolveCommand($command);
                if (!$cmd) {
                    $this->error(404);
                    exit();
                }
            } catch (\OroborosInstallerException $e) {
                $this->error($e->getCode());
                exit();
            }
            $worker = $this->_loadWorker($cmd->worker);
            $check = $worker->checkMethod($cmd->method);
            if ($check === TRUE) {
            try {
                $result = $worker->{$cmd->method}($args);
                if (isset($result) && $result) {
                    echo json_encode(array(
                        'status' => 200,
                        'data' => $result
                    ));
                    exit();
                }
                $this->error(500);
            } catch (\OroborosInstallerException $e) {
                $this->error(500, $e->getMessage());
            }
        } else {
            //return an access error
            $this->error(403);
        }
        exit();
        } else {
            return FALSE;
        }
    }

    public function error($code = 0, $data = NULL) {
        $error_meta = $this->_resolveError($code);
        echo json_encode(array(
            'status' => $code,
            'type' => $error_meta->type,
            'title' => $error_meta->title,
            'message' => $error_meta->message,
            'data' => $data
        ));
        exit();
    }
    
    protected function _envcheck(array $args = array()) {
        return '{}';
    }

    protected function _envtable(array $args = array()) {
        return '{}';
    }

    protected function _profile(array $args = array()) {
        return '{}';
    }
    
    private function _resolveCommand($command) {
        if (!isset($this->_schema->commands->{$command})) {
            return FALSE;
        }
        $allowed = $this->_schema->workers->{$this->_schema->commands->{$command}->worker}->allow;
        $method = $this->_schema->commands->{$command}->method;
        if (!in_array($method, $allowed)) {
            print_r($allowed); die();
            throw new \oroboros\src\lib\common\libraries\exception\OroborosException("Ajax access not allowed.", 403);
        }
        $cmd = $this->_schema->commands->{$command};
        return $cmd;
    }
    
    private function _resolveError($code) {
        $index = 'code_' . (string) $code;
        if (!isset($this->_errors->{$index})) {
            return FALSE;
        }
        $error = $this->_errors->{$index};
        return $error;
    }
    
    private function _loadWorker($name) {
        $class = self::WORKER_NAMESPACE . ucfirst((string) $name) . "Worker";
        $instance = new $class();
        $instance->initialize(array(
            'schema' => $this->_schema->workers->{$name},
            'conditions' => $this->_conditions->{$name}
        ));
        return $instance;
    }

}
