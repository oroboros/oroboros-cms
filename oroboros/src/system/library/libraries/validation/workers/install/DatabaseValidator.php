<?php

/*
 * The MIT License
 *
 * Copyright 2016 Brian Dayhoff <brian@mopsyd.me>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\src\system\library\libraries\validation\workers\install;

/**
 * Description of DatabaseValidator
 *
 * @author Brian Dayhoff <brian@mopsyd.me>
 */
final class DatabaseValidator extends \oroboros\src\lib\common\abstracts\libraries\validation\workers\AbstractFormValidationWorker {

    public function validate($data, array $schema) {
        $result = parent::validate($data, $schema);
        if (!$result->valid) {
            return $result;
        }
        $details = \oroboros\Common::cast('object', array(
                    'type' => $result->raw->{"database-type"},
                    'host' => $result->raw->{"database-host"},
                    'name' => $result->raw->{"database-name"},
                    'username' => $result->raw->{"database-username"},
                    'password' => $result->raw->{"database-password"},
                    'charset' => 'utf8',
        ));
        $connectable = (($details->type === 'mongo') ? $this->_checkMongo($details) : $this->_checkPdo($details));
        $result->valid = $result->verdict = $connectable;
        return $result;
    }

    private function _checkPdo(\stdClass $data) {
        try {
            $pdo = new \PDO($data->type
                    . ':host=' . $data->host
                    . ';dbname=' . $data->name
                    . ';charset=' . $data->charset, $data->username, $data->password, array(\PDO::ATTR_EMULATE_PREPARES => false,
                \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION));
            return TRUE;
        } catch (\Exception $e) {
            return FALSE;
        }
    }

    private function _checkMongo(\stdClass $data) {
        return FALSE;
    }

}
