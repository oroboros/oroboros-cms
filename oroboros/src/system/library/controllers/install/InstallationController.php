<?php

namespace oroboros\src\system\library\controllers\install;

/*
 * The MIT License
 *
 * Copyright 2016 Brian Dayhoff <brian@mopsyd.me>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * <Oroboros System Installation Controller>
 * This is the system installer controller. It guides users 
 * through the system installation process, either from the 
 * browser or the command line. 
 * 
 * This controller does not require the database or external 
 * data sources to function. It is intentionally wired 
 * differently than the rest of the core so recovery is 
 * possible in the event of database failure, routing bugs, etc.
 * @author Brian Dayhoff <brian@mopsyd.me>
 * @since 0.0.2a
 */
final class InstallationController extends \oroboros\src\lib\common\abstracts\controllers\AbstractController {

    const INSTALL_SCHEMA = '../../../config/install.schema.json';
    const INSTALL_PREFERENCES = '../../../config/install.preferences.json';

    private $_model;
    private $_controller;
    private $_router;

    /**
     * @deprecate Refactor to model/view for the most part
     */
    private $_config;
    private $_preferences;
    private $_installer;
    private $_environment;
    private $_validator;
    private $_initialized = FALSE;

    public function __construct(array $params = array(), array $flags = array()) {
        parent::__construct($params, $flags);
    }

    public function __destruct() {
        parent::__destruct();
    }

    public function initialize(array $params = array(), array $flags = array()) {
        try {
            parent::initialize($params, $flags);
            $this->_initializePreferences($params, $flags);
            $this->_initializeConfiguration($params, $flags);
            $this->_initializeLogger();
            $this->_initializeRouter();
            $this->_initializeModel();
            $this->_initializeValidator();
        } catch (\Exception $e) {
            $this->_log(self::CRITICAL, "Controller Failure: " . $e->getMessage());
            throw new \oroboros\src\lib\common\libraries\exception\OroborosException("Initialization step failed!", self::ERROR_CONTROLLER, $e);
        }
    }

    public function index(array $params = array(), array $flags = array()) {
        try {
            //Sets the new step if next/previous was passed from the client
            $this->_checkStepNavigationUpdate();
            //fetches the page schema
            $schema = $this->_schema($this->_model->step());
            $previous_data = $this->_model->stepProgressData($this->_model->step());
            $data = (isset($_POST) && !empty($_POST)) ? $_POST : ((isset($previous_data->data)) ? \oroboros\Common::cast('array', $previous_data->data) : array());
            //renders if applicable
            if (!isset($schema->validate) || !$schema->validate) {
                $this->_model->updateStepData($this->_model->step(), TRUE, $data);
            } else {
                $evaluation = $this->_validate(str_replace('-validate', NULL, $this->_model->step()), $data);
                if (!$evaluation) {
                    $this->index($params, $flags);
                    exit();
                }
            }
            if ($schema->render) {
                $this->_render($data);
                exit();
            }
            if ($schema->redirect) {
                $this->_handleRedirect();
                exit();
            }
        } catch (\oroboros\src\lib\common\libraries\exception\OroborosException $e) {
            //Load the error page
            $params = array(
                "title" => "Installation Error",
                "message" => "There has been an error during installation.",
                "code" => self::HTTP_SERVER_ERROR,
                "exception" => $e,
            );
            $this->error($params);
        }
        exit();
    }

    private function _checkStepNavigationUpdate() {
        
        if (isset($_POST['step'])) {
            $this->_model->setStep($_POST['step']);
            unset($_POST['step']);
        }
    }

    public function error(array $params = array(), array $flags = array()) {
        \dev\Utils::crash($params);
    }

    private function _handleRedirect() {
        $stepData = $this->_model->stepData();
        if (isset($stepData['redirect_location']) && $stepData['redirect_location']) {
            $this->_redirect($stepData['redirect_location']);
            exit();
        } else {
            $this->_model->setStep($stepData['redirect_step']);
            $this->index();
        }
    }

    private function _validate($mode, $data) {
        try {
            
            $validation = \oroboros\Common::cast('array', $this->_config->validation->forms->{$mode});
            $this->_validator->mode($mode);
            $result = $this->_validator->validate($data, $validation);
            $render = array( "verdict" => $result->valid, "post_data" => $data);
            $response = $result->valid;
            if (!$result->valid) {
                $errors = $this->_packageFormErrors($mode, $result->verdict);
                $this->_model->setStep($this->_model->previous());
                $this->_setParam('render', array("errors" => $errors, "post_data" => $data));
                $render["errors"] = $errors;
            }
            $this->_model->updateStepData($mode, $result->verdict, $data);
            $this->_model->updateStepData($this->_model->step(), $result->verdict);
            $this->_setParam('render', $render);
            return $response;
        } catch (\Exception $e) {
            \dev\Utils::crash($e->getMessage());
        }
    }

    protected function _redirect($location) {
        header("Location: " . $location);
        exit();
    }

    protected function _render(array $data = array()) {
        $this->_initializeView($this->_packageRenderData($data));
        $this->_view->render();
    }

    private function _packageFormErrors($type, $result) {
        $error_messages = $this->_config->errors->validation->forms->{$type};
        $errors = array();
        var_dump($result);
        var_dump($type);
        die();
        foreach ($result as $key => $value) {
            if (!$value) {
                $errors[$key] = $error_messages->{$key};
            }
        }
        return \oroboros\Common::cast('object', $errors);
    }

    private function _checkInitialization() {
        if (!$this->_initialized) {
            throw new \oroboros\src\lib\common\libraries\exception\OroborosException("Installer must be initialized before operating!", self::ERROR_INITIALIZATION);
        }
    }

    private function _initializePreferences(array $params = array(), array $flags = array()) {
        $file = realpath(__DIR__ . DIRECTORY_SEPARATOR . self::INSTALL_PREFERENCES);
        $preferencesRaw = file_get_contents($file);
        $this->_setParam('preferences', json_decode($preferencesRaw, JSON_UNESCAPED_SLASHES | JSON_OBJECT_AS_ARRAY));
        $this->_preferences = json_decode($preferencesRaw, JSON_UNESCAPED_SLASHES | JSON_OBJECT_AS_ARRAY);
    }

    private function _initializeConfiguration(array $params = array(), array $flags = array()) {
        $file = realpath(__DIR__ . DIRECTORY_SEPARATOR . self::INSTALL_SCHEMA);
        $configRaw = file_get_contents($file);
        $this->_setParam('configuration', json_decode($configRaw));
        $this->_config = json_decode($configRaw);
    }

    private function _initializeLogger($return = FALSE) {
        if ($this->_checkParam('logger')) {
            $logger = $this->_getParam('logger');
        } else {
            $logger = \oroboros\Oroboros::getLoggerObject();
            $logger->refreshOptions($this->_getParam('preferences')['log']);
        }
        $this->setLogger($logger);
        if ($return) {
            return $logger;
        }
    }

    private function _initializeRouter() {
        if ($this->_checkParam('router')) {
            $this->_router = $this->_getParam('router');
            if (!$this->_router->isInitialized()) {
                $this->_router->initialize();
            }
        } else {
            $this->_router = new \oroboros\src\system\library\libraries\dns\InstallationRouter();
            $this->_router->initialize();
        }
    }

    private function _initializeValidator() {
        if ($this->_checkParam('validator')) {
            $this->_validator = $this->_getParam('validator');
            $this->_validator->initialize($this->_config->validation);
        } else {
            try {
                $params = $flags = array();
                if (isset($this->_config->validation->forms->{$this->_model->step()})) {
                    $params['schema'] = get_object_vars($this->_config->validation->forms->{$this->_model->step()});
                }
                $this->_validator = $this->_load('library', 'system', 'validation\\InstallationValidator');
                $this->_validator->initialize($params, $flags);
            } catch (\Exception $e) {
                \dev\Utils::crash($e->getMessage());
            }
        }
    }

    private function _initializeModel(array $params = array(), array $flags = array()) {
        $params['schema'] = isset($params['schema']) ? $params['schema'] : $this->_config->steps;
        $params['router'] = isset($params['router']) ? $params['router'] : $this->_router;
        $params['logger'] = isset($params['logger']) ? $params['logger'] : $this->_initializeLogger(1);
        $this->_model = $this->_load('model', 'system', 'install\\InstallationModel', $params, $flags);
        $this->_model->initialize($params, $flags);
    }

    private function _initializeView(array $params = array(), array $flags = array()) {
        $this->_view = $this->_load('view', 'system', 'install\\InstallationView', $params, $flags);
    }

    private function _schema($step = FALSE) {
        $step = (($step) ? $step : $this->_model->step());
        $schema = $this->_config->steps->{$step};
        return $schema;
    }

    private function _config() {
        return $this->_config->config;
    }

    private function _resources() {
        return $this->_config->resources;
    }

    private function _packageRenderData(array $data = array()) {
        $result = array(
            'title' => $this->_config->templates->body->{$this->_model->step()}->title,
            'config' => $this->_config(),
            'schema' => $this->_schema(),
            'resources' => $this->_resources(),
            'page' => $this->_model->stepData(),
            'step' => $this->_model->step(),
            'step_next' => $this->_model->next(),
            'step_previous' => $this->_model->previous(),
            'steps_all' => $this->_model->steps(1),
            'metadata' => $this->_packageJavascriptMetadata(),
            'data' => $data,
            'post_values' => ((empty($_POST)) ? FALSE : $_POST),
        );
        /**
         * Non-destructively expose the View to contents of the render 
         * initialization param, if it exists.
         */
        if ($this->_checkParam('render')) {
            foreach ($this->_getParam('render') as $key => $value) {
                $result[$key] = $value;
            }
        }
        return $result;
    }

    private function _packageJavascriptMetadata() {
        return array(
            "app" => array(
                "step" => array(
                    "current" => $this->_model->step(),
                    "previous" => $this->_model->previous(),
                    "next" => $this->_model->next()
                ),
                "ajax" => array(
                    "url" => OROBOROS_URL,
                ),
            ),
            "page" => array(),
            "user" => array(),
            "security" => array(
                "permissions" => array(),
                "nonce" => "TODO"
            )
        );
    }

}
