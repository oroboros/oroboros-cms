<?php

namespace oroboros\src\system\library\controllers\install;

/*
 * The MIT License
 *
 * Copyright 2016 Brian Dayhoff <brian@mopsyd.me>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * <Oroboros System Installation Controller>
 * This is the system installer controller. It guides users 
 * through the system installation process, either from the 
 * browser or the command line. 
 * 
 * This controller does not require the database or external 
 * data sources to function. It is intentionally wired 
 * differently than the rest of the core so recovery is 
 * possible in the event of database failure, routing bugs, etc.
 * @author Brian Dayhoff <brian@mopsyd.me>
 * @since 0.0.2a
 */
final class InstallationAjaxController extends \oroboros\src\lib\common\abstracts\controllers\AbstractAjaxController {

    const DEFAULT_ERROR_CODE = self::HTTP_SERVER_ERROR;
    const INSTALL_SCHEMA = 'config/install.schema.json';
    const INSTALL_PREFERENCES = 'config/install.preferences.json';

    private $_logger;

    public function __construct(array $params = array(), array $flags = array()) {
        parent::__construct($params, $flags);
        $configRaw = file_get_contents(OROBOROS_SYSTEM
                . self::INSTALL_SCHEMA);
        $preferencesgRaw = file_get_contents(OROBOROS_SYSTEM
                . self::INSTALL_PREFERENCES);
        $this->_setConfig(json_decode($configRaw));
        $this->_setPreferences(json_decode($preferencesgRaw, JSON_UNESCAPED_SLASHES | JSON_OBJECT_AS_ARRAY));
        //Initialize installation dependencies
        $this->_logger = \oroboros\Oroboros::getLoggerObject();
        $this->_logger->refreshOptions($this->_preferences('log'));
    }

    public function initialize(array $params = array(), array $flags = array()) {
        try {
            parent::initialize($params, $flags);
            $this->_router = $this->_load('library', 'system', 'dns\\InstallationRouter', $params, $flags);
            $this->setLogger($this->_logger);
            $this->_loadModel();
            $this->_loadView();
        } catch (\Exception $e) {
            $this->_log(self::ERROR, 'Error during AJAX controller initialization: [' . $e->getMessage() . ']' . PHP_EOL . '--->' . $e->getTraceAsString());
            $this->error(array('code' => self::HTTP_SERVER_ERROR));
            exit();
        }
    }

    public function index(array $params = array(), array $flags = array()) {
        if (!array_key_exists('command', $this->_request())) {
            //unknown command
            echo 'Unknown Command' . PHP_EOL;
            $this->error(array('code' => self::HTTP_BAD_REQUEST));
        } elseif (!$this->_model->checkCommand($this->_request()['command'])) {
            //malformed request
            echo 'Malformed Request:' . PHP_EOL . print_r($this->_request()['command'], 1) . PHP_EOL;
            $this->error(array('code' => self::HTTP_SERVER_ERROR));
        } else {
            try {
                //process the request
                $response = $this->_model->command($this->_request()['command'], isset($this->_request()['args']) ? $this->_request()['args'] : array());
                if ($response) {
                    //render the result
                    http_response_code(200);
                    $this->_view->mode($this->_responseType());
                    $this->_view->render($this->_packageRenderData(array()));
                } else {
                    //model could not fulfill request
                    echo 'Model Failure' . PHP_EOL;
                    $this->error(array('code' => self::HTTP_SERVER_ERROR));
                }
            } catch (\Exception $e) {
                //Logic error, pass back 500 status code
                echo 'Logic Error:' . $e->getMessage() . PHP_EOL;
                $this->error(array('code' => self::HTTP_SERVER_ERROR));
            }
        }
        exit();
    }

    private function _loadView() {
        $this->_view = $this->_load('view', 'common', 'AjaxView');
    }

    private function _loadModel() {
        $this->_model = $this->_load('model', 'system', 'install\\InstallationAjaxModel', array(
            'schema' => $this->_config()->ajax,
            'errors' => $this->_config()->errors->ajax,
            'conditions' => $this->_config()->validation,
            'router' => $this->_router()
        ));
    }

}
