<?php

namespace oroboros\src\system\index\install\workers;

/*
 * The MIT License
 *
 * Copyright 2016 Brian Dayhoff <brian@mopsyd.me>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * 
 */
final class BuildWorker extends \oroboros\src\lib\common\abstracts\libraries\AbstractWorker {

    private $_schema;
    private $_logCache = [];
    private $_conditions;
    private $_required;
    private $_evaluation = [];

    public function __construct(array $params = array(), array $flags = array()) {
        parent::__construct($params, $flags);
    }

    public function initialize(array $params = array(), array $flags = array()) {
        parent::initialize($params, $flags);
    }

    protected function _setSchema(\stdClass $schema) {
        $this->_schema = $schema;
    }

    protected function _setConditions($conditions) {
        $this->_conditions = $conditions;
        $this->_updateFilebaseConditions();
    }

    private function _updateFilebaseConditions() {
        $this->_required["filebase"]["readable"] = [];
        $this->_required["filebase"]["writeable"] = [
            OROBOROS_BASEPATH,
            OROBOROS,
            OROBOROS_TMP,
            OROBOROS_BIN,
            OROBOROS_CACHE,
            OROBOROS_APPS,
            OROBOROS_CONFIG,
            OROBOROS_DATA,
            OROBOROS_RESOURCES,
        ];
    }

    public function checkMethod($method) {
        return ((!method_exists($this, $method)) ? 404 : ((!in_array($method, $this->_schema->allow)) ? 403 : TRUE));
    }

    public function evaluate($args = NULL) {
        try {
            $this->_evaluation = array(
                "fingerprint" => $this->_checkFingerprint(),
                "apache" => $this->_checkApache(),
                "cli" => $this->_checkCli(),
                "php" => $this->_checkPhp(),
                "composer" => $this->_checkComposer(),
                "database" => $this->_checkDatabase(),
                "filebase" => $this->_checkFilebase(),
                "git" => $this->_checkGit(),
                "perl" => $this->_checkPerl(),
                "python" => $this->_checkPython(),
                "ruby" => $this->_checkRuby(),
                "node" => $this->_checkNode(),
                "npm" => $this->_checkNpm(),
                "bower" => $this->_checkBower(),
            );
        } catch (\Exception $e) {
            $this->_log("[ALERT] Server Evaluation Failure: " . $e->getMessage());
            return false;
        }
        return true;
    }

    public function tabulate(array $args = array()) {
        if (!empty($args)) {
            print_r($args);
            //tabulate based on provided criteria
        } else {
            //tabulate everything
            $categories = array_keys(\oroboros\Common::cast('array', $this->_conditions));
            $catalog = array();
            foreach ($categories as $index) {
                if (isset($this->_conditions->{$index}->_meta) && $this->_conditions->{$index}->_meta->enabled) {
                    $catalog[$index] = array(
                        "meta" => $this->_conditions->{$index}->_meta,
                        "tasks" => new \stdClass()
                    );
                    $tasks = ((isset($this->_conditions->{$index}->tasks)) ? $this->_conditions->{$index}->tasks : new \stdClass());
                    foreach ($tasks as $task_index => $task) {
                        if (isset($task->_meta)) {
                            $taskMeta = array(
                                "title" => $task->_meta->title,
                                "message" => $task->_meta->message
                            );
                            $catalog[$index]['tasks']->{$task_index} = $taskMeta;
                        }
                    }
                };
            }
            return $catalog;
        }
    }

    public function check($keyword) {
        
    }

    public function profile($params) {
        $method = '_check' . ucfirst($params['category']);
        return $this->{$method}($params['index']);
    }

    private function _checkFingerprint() {
        
    }

    private function _checkApache($spec = FALSE) {
        switch ($spec) {
            default:
                return FALSE;
                break;
        }
    }

    private function _checkPhp($spec = FALSE) {
        switch ($spec) {
            case 'version':
                //compare the current version to the expected version
                return version_compare(PHP_VERSION, '5.4.0') !== -1 ? TRUE : FALSE;
                break;
            case 'required':
                //compare the current version to the expected version
                return version_compare(PHP_VERSION, '5.4.0') !== -1 ? TRUE : FALSE;
                break;
            default:
                return FALSE;
                break;
        }
        $diagnosis = $this->_phpProfile();
        $results = array();
        foreach ($this->_conditions->php->tasks as $task => $details) {
            $res = array();


            $results[$task] = $res;
        }
        exit();
    }

    private function _phpProfile() {
        return \oroboros\Common::cast("object", array(
                    "version" => PHP_VERSION,
                    "modules" => array(
                        "native" => get_loaded_extensions(),
                        "zend" => get_loaded_extensions(1)
                    ),
        ));
    }

    private function _checkDatabase($spec = FALSE) {
        switch ($spec) {
            case 'mysql':
            case 'mssql':
            case 'oracle':
            case 'postgre':
            case 'sqlite':
                break;
            case 'mongo':
                break;
            default:
                return FALSE;
                break;
        }
    }

    private function _checkFilebase($spec = FALSE) {
        switch ($spec) {
            case 'read':
                //check that all system directories are readable
                $results = array();
                $errors = array();
                $path = OROBOROS_BASEPATH;
                $objects = new \RecursiveIteratorIterator(
                        new \RecursiveDirectoryIterator($path), \RecursiveIteratorIterator::SELF_FIRST);
                $details = array();
                foreach ($objects as $name => $object) {
                    if ((strpos($name, '/.') === FALSE) && $object->isDir()) {
                        $details[$object->getRealPath()] = $object->isReadable();
                    }
                }
                return in_array(FALSE, $details);
                break;
            case 'write':
                //check that all system directories are writable
                $results = array();
                $errors = array();
                $path = realpath(OROBOROS_BASEPATH);
                $objects = new \RecursiveIteratorIterator(
                        new \RecursiveDirectoryIterator($path), \RecursiveIteratorIterator::SELF_FIRST);
                $details = array();
                foreach ($objects as $name => $object) {
                    if ((strpos($name, '/.') === FALSE) && $object->isDir()) {
                        $details[$object->getRealPath()] = $object->isWritable();
                    }
                }
                return in_array(FALSE, $details);
                break;
            default:
                return FALSE;
                break;
        }
    }

    private function _checkVersioncontrol($spec = FALSE) {
        switch ($spec) {
            case 'check':
                $check = self::_exec('git --version');
                return (strpos($check, 'git version') !== FALSE);
                die();
                break;
            default:
                return FALSE;
                break;
        }
    }

    private function _checkWebserver($spec = FALSE) {
        switch ($spec) {
            case 'type':
                return strpos(strtolower($_SERVER['SERVER_SOFTWARE']), 'apache') !== FALSE;
                break;
            default:
                return FALSE;
                break;
        }
    }

    private function _checkServer($spec = FALSE) {
        switch ($spec) {
            default:
                return FALSE;
                break;
        }
    }

    private function _checkGit($spec = FALSE) {
        switch ($spec) {
            default:
                return FALSE;
                break;
        }
    }

    private function _checkPerl($spec = FALSE) {
        switch ($spec) {
            default:
                return FALSE;
                break;
        }
    }

    private function _checkPython($spec = FALSE) {
        switch ($spec) {
            default:
                return FALSE;
                break;
        }
    }

    private function _checkRuby($spec = FALSE) {
        switch ($spec) {
            default:
                return FALSE;
                break;
        }
    }

    private function _checkNode($spec = FALSE) {
        switch ($spec) {
            default:
                return FALSE;
                break;
        }
    }

    private function _checkNpm($spec = FALSE) {
        switch ($spec) {
            default:
                return FALSE;
                break;
        }
    }

    private function _checkBower($spec = FALSE) {
        switch ($spec) {
            case 'type':
                $check = self::_exec('bower --version');
                if (preg_match('/[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}/', $check)) {
                    //native support - pass
                    return TRUE;
                }
                $path = OROBOROS_BIN . 'phar' . DIRECTORY_SEPARATOR . 'bowerphp.phar';
                $check = self::_exec($path . ' --version');
                if (preg_match('/[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}/', $check)) {
                    //emulated support - pass, with warnings
                    return TRUE;
                }
                //Fail
                return FALSE;
                break;
            default:
                return FALSE;
                break;
        }
    }

    private function _checkComposer($spec = FALSE) {
        switch ($spec) {
            case 'type':
                $check = self::_exec('composer --version');
                if (strpos($check, 'Composer version') !== FALSE) {
                    //native support - pass
                    return TRUE;
                }
                $path = OROBOROS_BIN . 'phar' . DIRECTORY_SEPARATOR . 'composer.phar';
                $check = self::_exec($path . ' --version');
                if (strpos($check, 'Composer version') !== FALSE) {
                    //emulated support - pass, with warnings
                    return TRUE;
                }
                //Fail
                return FALSE;
                break;
            default:
                return FALSE;
                break;
        }
    }

    private function _checkCli($spec = FALSE) {
        switch ($spec) {
            default:
                return FALSE;
                break;
        }
    }

    private function _checkCron($spec = FALSE) {
        switch ($spec) {
            default:
                return FALSE;
                break;
        }
    }

    private function _checkBash($spec = FALSE) {
        switch ($spec) {
            default:
                return FALSE;
                break;
        }
    }

    private function _checkDos($spec = FALSE) {
        switch ($spec) {
            default:
                return FALSE;
                break;
        }
    }

    private static function _exec($command) {
        $handle = popen(escapeshellcmd($command) . ' 2>&1', 'r');
        $result = fread($handle, 2096);
        pclose($handle);
        return $result;
    }

}
