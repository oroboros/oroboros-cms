<?php

namespace oroboros\src\system\index\install\workers;

/*
 * The MIT License
 *
 * Copyright 2016 Brian Dayhoff <brian@mopsyd.me>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * 
 */
final class DatabaseWorker extends \oroboros\src\lib\common\abstracts\libraries\AbstractWorker {

    private $_pdo;
    private $_mongo;

    public function validate($data, $schema = array()) {
        parent::validate($data, $schema);
    }

    public function execute() {
        parent::execute();
    }
    
    public function checkMethod($method) {
        return ((!method_exists($this, $method)) ? 404 : ((!in_array($method, $this->_schema->allow)) ? 403 : TRUE));
    }
    
    private function _buildPdoConnectionObject(array $data) {
        
    }

    private function _checkPdo(\stdClass $data) {
        $pdo = new \PDO($data->type
                . ':host=' . $data->host
                . ';dbname=' . $data->name
                . ';charset=' . $data->charset, $data->username, $data->password, array(PDO::ATTR_EMULATE_PREPARES => false,
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));

        print_r($pdo);
        die();
    }

    private function _checkMongo(\stdClass $data) {
        
    }

}
