<?php

namespace oroboros\src\system\index\install\workers;

/*
 * The MIT License
 *
 * Copyright 2016 Brian Dayhoff <brian@mopsyd.me>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * 
 */
final class ComposerWorker extends \oroboros\src\lib\common\abstracts\libraries\AbstractWorker {

    private $_default_state = "fail";
    private $_valid_states = array(
        "pass",
        "fail",
        "emulate",
    );
    
    protected function initialize($schema) {
        $this->_registerStates($this->_valid_states);
        $this->_state($this->_default_state);
        parent::initialize($schema);
    }

    /**
     * Validates whether or not composer exists on the server.
     * @param type $data
     * @return type
     */
    public function validate($data = NULL) {
        parent::validate($data);
        try {
            
        } catch (\Exception $e) {
            
        }
    }

    /**
     * installs a local copy of composer if it is not available through the system
     * @return void
     */
    public function execute() {
        parent::execute();
    }

    protected function _onStateChange() {
        if ($this->_state === "emulate") {
            copy('https://getcomposer.org/installer', OROBOROS_BIN . 'composer-setup.php');
            if (hash_file('SHA384', OROBOROS_BIN . 'composer-setup.php') === '070854512ef404f16bac87071a6db9fd9721da1684cd4589b1196c3faf71b9a2682e2311b36a5079825e155ac7ce150d') {
                echo 'Installer verified';
                require_once "composer-setup.php";
                unlink(OROBOROS_BIN . 'composer-setup.php');
            } else {
                echo 'Installer corrupt';
                unlink(OROBOROS_BIN . 'composer-setup.php');
            }
        }
    }

}
