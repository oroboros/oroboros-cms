<?php
namespace oroboros\src\system\index\install;
/*
 * The MIT License
 *
 * Copyright 2016 Brian Dayhoff <brian@mopsyd.me>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * <Oroboros Core System Installer Bootstrap>
 *
 * @author Brian Dayhoff <brian@mopsyd.me>
 */
final class InstallationBootstrap extends \oroboros\src\lib\common\abstracts\libraries\startup\AbstractBootstrap {
    
    private $_controller;
    
    public function launch() {
        try {
            $this->_controller->index();
        } catch(\oroboros\src\lib\common\libraries\exception\OroborosException $e) {
            $this->_controller->error();
        }
        
    }
    
    public function initialize(array $params = array(), array $flags = array()) {
        parent::initialize($params, $flags);
        $this->_loadResources();
        if ($this->_controller->initialize($params, $flags)) {
            return $this;
        }
        return FALSE;
    }
    
    private function _loadResources() {
        //Load Installation Resources
        $fileload = glob(OROBOROS_INSTALL_RESOURCES . "installer" . DIRECTORY_SEPARATOR . "*.php");
        \array_map(function($file) {
            \oroboros\src\lib\common\require_safely($file);
        }, $fileload);
        $class = (($this->_checkFlag(self::FLAG_MODE_AJAX)) ? 'install\\InstallationAjaxController' : (($this->_checkFlag(self::FLAG_MODE_CLI)) ? 'install\\InstallationCliController' : 'install\\InstallationController' )  );
        $this->_controller = $this->_load("controller", "system", $class);
        $this->_controller->initialize();
    }
}
