<?php
/**
 * Basic runtime definitions to ease bootstrapping and installation.
 */
define("OROBOROS_SRC", OROBOROS . 'src' . DIRECTORY_SEPARATOR);
define("OROBOROS_DATA", OROBOROS . "data" . DIRECTORY_SEPARATOR);
define("OROBOROS_CACHE", OROBOROS . "cache" . DIRECTORY_SEPARATOR);
define("OROBOROS_CONFIG", OROBOROS . "config" . DIRECTORY_SEPARATOR);
define("OROBOROS_TMP", OROBOROS . "tmp" . DIRECTORY_SEPARATOR);
define("OROBOROS_BIN", OROBOROS . "bin" . DIRECTORY_SEPARATOR);
define("OROBOROS_APPS", OROBOROS . "apps" . DIRECTORY_SEPARATOR);
define("OROBOROS_LIB", OROBOROS_SRC . "lib" . DIRECTORY_SEPARATOR);
define("OROBOROS_DEPENDENCIES", OROBOROS_LIB . "dependencies" . DIRECTORY_SEPARATOR);
define("OROBOROS_SYSTEM", OROBOROS_SRC . "system" . DIRECTORY_SEPARATOR);
define("OROBOROS_RESOURCES", OROBOROS_LIB . "resources" . DIRECTORY_SEPARATOR);
define("OROBOROS_CORE", OROBOROS_LIB . "core" . DIRECTORY_SEPARATOR);
define("OROBOROS_LOCAL", OROBOROS_LIB . "local" . DIRECTORY_SEPARATOR);
define("OROBOROS_STAGING", OROBOROS_LIB . "staging" . DIRECTORY_SEPARATOR);
if (PHP_SAPI == "cli") {
    /**
     * In cli-mode, some superglobals not accessible. This will provide safely
     * nulled definitions that prevent system-failing bugs and default from HTML
     * to plaintext display.
     */
    define("OROBOROS_URL", FALSE);
    define("OROBOROS_PAGE", FALSE);
    define("OROBOROS_CLI", TRUE);
} else {
    // Not in cli-mode, use HTTP protocol, flag CLI mode as false.
    define("OROBOROS_URL", 'http'
            //checks for SSL (small hack to validate on IIS servers)
            . ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== "off") 
            ? 's' 
            : NULL)
            . '://' . $_SERVER['HTTP_HOST']);
    define("OROBOROS_PAGE", OROBOROS_URL
            . ( (isset($_SERVER['REQUEST_URI']) && $_SERVER['REQUEST_URI'] !== '/' ) 
            ? $_SERVER['REQUEST_URI'] 
            : '/' ));
    define("OROBOROS_CLI", FALSE);
}