<?php
namespace oroboros\app\OroborosApp;
if (!defined("APPLICATION")) {

	define("APPLICATION", "OroborosApp");
	define("APPLICATION_PATH", realpath(dirname(__FILE__)));
	
	final class OroborosApp extends oroboros\Application implements oroboros\api\ApplicationInterface {
		
		final public function __construct() {
			parent::__construct();
			$this->bootstrap();
		}

		final public function __destruct() {
			parent::__destruct();
		}
		
	}
} else {
	throw new OroborosException("[CRITICAL] Application is already declared!" . PHP_EOL . ">Current application defined as: " . APPLICATION . PHP_EOL . ">Aborting execution.", 500);
}

