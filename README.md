# Oroboros #

**Version 0.0.1a (*alpha build*)**

---

Oroboros is a family of loosely coupled resources geared at rapid web app development and easy ongoing maintainability. All components of Oroboros can be run as individual standalone modules. The full system provides a lightweight, unopinionated Model-View-Adapter framework for building and deploying apps, and is geared primarily toward rapid prototyping, development and deployment of these applications. It also can be used to migrate apps between servers, wrap other applications for debugging or modification, and connect to other installations and services to create SaaS services or decentralized servers. The full framework is intended to only run in a development environment, and should not be used on a production server directly. Oroboros will export it's component dependencies when deploying an app, so use it on your home or work machine and let it live there. Deployment can be done via FTP, SSH, or Git *(git deployment requires some complicated setup on your production server if you are pushing there via git, but regular repos are a cinch)*.

This application represents the *full core build,* and can pull in the other related repositories during installation. If you only require a few component pieces of Oroboros for your project, please see the information on our related repositories *located in the LINKS.md section.*

---

## Features ##

* @TODO
* @TODO
* @TODO

---

## Dependencies ##

* **PHP 5.4.0+** *(5.5+ is optimal, otherwise patched for compatibility)*
* **MySQL / MariaDB** *(support for PostgreSQL, Oracle, MSSQL, Sqlite, and MongoDB coming shortly)*
* **Apache** *(support for Nginx and IIS coming shortly)*
* **Git** *(optional, but recommended)*
* **Composer** *(optional, but recommended)*
* **Bower** *(optional, but recommended)*

---

## Installation ##

<sup>__NOTE: Your base install directory must be writeable to the webserver. The installer will correct file permissions after a few setup tasks.__</sup>

#### Getting the installation files ####


#### Browser Mode (WIP) ####

@TODO

---

#### Command Line ####


###### Mac / Unix - via shell script (WIP) ######

```bash
cd path/to/oroboros/
oroboros/bin/install.sh
```

###### Windows - via batch file (WIP) ######

```dos
cd path/to/oroboros/
oroboros/bin/install.bat
```

---

## Specifications ##

Below is a list of criteria used for development of Oroboros to help you find what you are looking for. Well structured projects built on this platform should follow these standards.

### File Schema Specifications ###

* __All classes__ live in a single file by themselves.
* __All files containing classes__ are BruteCased. These should be the only files that start with capital letters.
* __All files that CAN execute changes simply by being included__ are to be named as such: *filename.routine.php*, or *filename.routine.yourlanguagesuffix (eg: cron.routine.sh, backup.routine.bat, sometask.routine.pl, etc).* If multiple file extensions are required, they should go after the routine bit.
* __All files that CAN execute changes simply by being included *(routines)*__ MUST NOT make function or class declarations, in accordance with PSR-1 standards.
* __All routines__ should live in the *oroboros/routines* directory for the base installation, or for apps, in *APPNAME/app/library/routines*.
* __All non-vendor classes and functionsets__ are namespaced. The namespace translates to their filepath, from the oroboros directory (not the base install directory, one level in from that).
* __All dependencies live in *oroboros/dependencies*__ The composer directory is *oroboros/dependencies/vendor*, for example. Other 3rd party services that manage dependencies MUST be configured to place their primary file collection folder in this directory. Individual apps that are built by the system have their own equivalent dependency directory, and should use that instead.
* __All template files__ are to be named with the *.phtml suffix*. The system templating engine WILL NOT include any template file that uses the standard *.php* suffix for security reasons.
* __All boilerplate files__ should be named like *filename.boilerplate*. Building from these will not execute any code placed therein, and is considerably safer than including any template that executes code. The system will include these files in such a way where they are not executed. If testing boilerplate, it will write to a temporary file and include that instead. If scaffolding an app, it will write the boilerplate to a new file with the appropriate extension, at which point it can be executed. No use of *eval()* occurs within this system.
* __All 3rd party modules live in *oroboros/src/modules*__. Placing them elsewhere will not be recognized by the system.
* __All images should exist only in *oroboros/data/uploads/img*__, which has script execution disabled for security. This is one of the most common ways that web software gets hacked; by scripts disguised as images. When building an app, your images will be moved to *APPNAME/client/resources/media/images*, where the entire media directory is non-executable.
* __No additional files should be added to the base installation root directory__ that directly execute ongoing program logic. Only webserver and development configuration files *(eg: .htaccess, web.config, composer.json, etc.)* should exist here. The system has it's own home for many of these documents, and will ignore the ones in the root directory entirely for the purpose of interal operations.
* __The *oroboros/bin* directory should only contain files that are command line executeable, or patches to make php perform at a higher level than the current version.__ No other files should EVER be in this directory.
* __All temporary files should go in *oroboros/tmp*__. This includes (but is not limited to) all of the following: image and media uploads, temporary config files, custom session files, etc. Everything in this directory should be able to be completely purged at any time with extremely minor inconvenience. Your custom code should process this data and delete it as quickly as possible, and typically only leave files here that are from a multi-page processing effort *(like an enormous database export, for example)*.
* __All custom configuration files MUST go in *oroboros/config*__. Modules should not save their config in their own directory. This way, updates can erase the entire module before proceeding without disrupting existing settings, and rollbacks of specific module settings can occur with minimal fuss.
* __All RESTful web services MUST live in *oroboros/services*__. Backend, internal SaaS services live in *oroboros/services/local*. Publically accessible services live in *oroboros/services/global*. If you are not using the integrated AJAX controller, you will need to put your custom AJAX landing file here also.
* __All services must have a filename corresponding to the command used to access them.__ Example: *http://mysite.com/services/customthing* maps to: *oroboros/services/customthing.php*.
* __Grouped services should be in a folder in *oroboros/services/[local|global]* that supplies the service vendor name as the folder name__. Example: *http://yoursite.com/services/google/authorize* and *http://yoursite.com/services/google/getemail* should map to *oroboros/services/google/authorize.php* and *oroboros/services/google/getemail.php* respectively.
* __Services your app provides MUST live in *APPNAME/app/services*__. Services that your app provides *to other apps on the same domain* go in *APPNAME/app/services/local*. Services your app provides over the general web go in *APPNAME/app/services/global*.
* __Projects using manually included components go in *oroboros/projects*__. This is anything you have entirely hand-rolled with no scaffolding support whatsoever. This is where small, simple projects live, and they are not likely to be interoperable with the app manager.
* __Apps generated programmatically by Oroboros go in *oroboros/apps*__. Anything pre-built by Oroboros itself lives here. There are development and production folders for ongoing development or testing a release version before deploying. The shared directory is for *multicasting*, for a future update.

---

## Support / Contact ##

*Please keep in mind that this is currently in an alpha state. The codebase is subject to change without notice, and support will be minimal until a beta version is ready for release. Bug reports are appreciated.*

##### NOTE: Our team is not currently looking for additional developers. We want the alpha architecture to be completed before taking on any additional support. We will open development up to the broader community when we reach the beta phase. Please check back periodically if you are interested in helping with development. ####

* [Bugs and Issue Reporting](bitbucket.org/oroborosframework/oroboros-core-alpha/issues)
* [Wiki and Documentation](bitbucket.org/oroborosframework/oroboros-core-alpha/wiki)