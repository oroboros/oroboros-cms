<?php
namespace dev;
/* 
 * The MIT License
 *
 * Copyright 2016 Brian Dayhoff <brian@mopsyd.me>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

final class Utils {
    
    const PATH = __FILE__;
    
    private static $_registry = [];
    private static $_errors = [];
    private static $_shutdown = [];
    
    public function __construct() {
        ;
    }
    
    public function __destruct() {
        ;
    }
    
    
    /* -------- Utility Methods --------*/
    
    public static function info() {
        $info = new \stdClass();
        $info->name = "Oroboros Development Utilities";
        $info->version = "v. 0.0.1-a";
        $info->classname = __CLASS__;
        $info->location = self::PATH;
        return $info;
    }
    
    public static function initialize() {
        register_shutdown_function(array(new \dev\Utils(), 'shutdown'));
    }
    
    public static function stash($key, $value) {
        
    }
    
    public static function fetch ($key, $source = NULL) {
        
    }
    
    /**
     * Breaks Execution, shows line number and file where called in output message
     * Can also optionally display additional data
     * @param mixed $data Any data to evaluate upon exit. Will be dumped in a legible format.
     */
    public static function crash($data = NULL, $backtrace = FALSE) {
        $output = NULL;
        $trace = debug_backtrace();
        $first = $trace[0];
        $message='<div class="devUtils_crash-message"'
                . 'style="position:absolute !important; left:0 !important; '
                . 'right:0 !important; width:100% !important; '
                . 'min-height: 100% !important; overflow: auto !important; '
                . 'background-color:white !important; color:black !important; '
                . 'z-index:9999 !important; display: block !important; font-family: monaco, profont, calibri \"Droid Sans Mono\", sans-serif;">'
                . '<blockquote ><div style="background-color: yellow; box-sizing:border-box; padding: 1ex 1em;"><h1>Program Terminated</h1>';
        $message .= '<super>At <strong>line: ' 
                . ((isset($first['line'])) ? $first['line'] : '[unknown_line]') . '</strong> of <strong>file: ' 
                . ((isset($first['file'])) ? $first['file'] : '[unknown_file]') . '</strong></super></div><hr>';
        if (isset($data)) {
            $message .= '<div class="devUtils_crash-data-eval">'
                    . '<h2>Additional data supplied at termination: </h2>'
                    . '<pre>';
            
            if (is_null($data) || is_int($data) || is_bool($data) || is_resource($data)) {
                ob_start();
                $message .= ob_get_clean() . '</pre><hr></div>';
            } elseif(is_object($data) || is_array($data)) {
                $message .= str_replace(PHP_EOL, '<br>' . PHP_EOL, print_r($data, 1)) . '</pre><hr></div>';
            } else {
                $message .= str_replace(PHP_EOL, '<br>' . PHP_EOL, (string) $data) . '</pre><hr></div>';
            }
            
        }
        if ($backtrace) {
            $message .= '<div class="devUtils_crash-backtrace">'
                    . '<h2>Backtrace: </h2>'
                    . '<pre>';
            ob_start();
            $trace = debug_backtrace();
            var_dump($trace);
            $message .= ob_get_clean() . '</pre><hr></div>';
        }
        $message .= '</blockquote></div>';
        die($message);
    }
    
    public static function onShutdown($name, $function) {
        if (!is_string($name)) {
            throw new \ErrorException('Shutdown Registry [$name] requires a (string)');
        }
        if (!is_string($function) && !is_callable($function) && !is_array($function)) {
            throw new \ErrorException('Shutdown Registry [$function] is not valid! ' 
                    . PHP_EOL . 'Please provide one of: ' 
                    . PHP_EOL . ' • A valid function name,'
                    . PHP_EOL . ' • An array, containing: array((object) $valid_object, (string) \'validPublicMethodFromObject\')'
                    . PHP_EOL . ' • Any callable variable (eg: $closure = function(){ ... };)'
                    );
        }
        self::$_shutdown[$name] = $function;
        return TRUE;
    }
    
    public static function shutdown() {
        foreach (self::$_shutdown as $shutdownIndex => $func) {
            if (is_callable($func)) {
                call_user_func($func);
            } elseif (is_array($func) && is_object($func[0]) && is_string($func[1])) {
                $func[0]->{$func[1]}();
            } elseif (is_string($func)) {
                $func();
            }
        }
    }
    
    public static function debug_format($input, $var = FALSE, $title = NULL) {
    if ($var)
        ob_start();
    echo '<span class="oroboros-debug-format" style="letter-spacing:initial;">' 
    . ((isset($title)) ? '<h1 class="oroboros-debug-title">' . $title . '</h1>' : NULL) . '<pre>';
    var_dump($input);
    echo '</pre></span>';
    if ($var)
        return ob_get_clean();
}
    
    
}
\dev\Utils::initialize();