<?php

/*
 * The MIT License
 *
 * Copyright 2016 Brian Dayhoff <brian@mopsyd.me>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * <Oroboros Standard Index>
 * This file starts the system. 
 * If you wish to start it from elsewhere and bypass this file, the baseline 
 * definitions declared here MUST direct back to this directory, and MUST end 
 * in a trailing slash. Provided that condition has been met, you may bootstrap 
 * from elsewhere. This is an effort to support future multicasting efforts for 
 * distributed servers, which may need to run a configuration routine before
 * starting the system.
 */
/**
 * <DO NOT REMOVE THESE. THEY MUST BE DEFINED FOR YOUR INSTALLATION TO RUN.>
 */
define("OROBOROS_BASEPATH", realpath(dirname(__FILE__)) . DIRECTORY_SEPARATOR);
define("OROBOROS", OROBOROS_BASEPATH . 'oroboros' . DIRECTORY_SEPARATOR);

/**
 * If you have any dependencies that need to
 * be executed BEFORE the system initializes,
 * put them here.
 */

/**
 * @codeCoverageIgnore
 *  not intended for final release
 */
require_once 'devlib.php';

/**
 * Uncomment these definitions to change how the system works:
 */
/**
 * <Enable Debugging Features Manually>
 * @wip (sorta)
 */
//define("OROBOROS_DEBUG", TRUE); //Enables debug mode. This is the master switch for all other debug functions.

/**
 * <Overrides The Router>
 * @wip
 * Force adherence to a single DNS point of entry. 
 * Use this to help fix network derps.
 * If you want this to be automatically calculated 
 * as your IPv4 address and path from the webroot, 
 * enter [true] as the second param. Many servers 
 * default to this pattern if there is no DNS for 
 * a site, which will break your app if you just 
 * use $_SERVER["HTTP_HOST"].
 * 
 * (example: http://127.0.0.1~/yourhomedirectory/ - Common on shared hosting);
 */
//define("OROBOROS_FORCE_DNS", "[http://your-custom-url-or-ip-address-here.com]");

/**
 * <Enables Unit Testing>
 * @wip
 * Enables unit testing mode.
 * The system will need to be manually loaded in 
 * your test, but definitions and autoloading will 
 * be present beforehand. Tests will need to go in 
 * the tests directory, located in the installation 
 * root directory.
 */
//define("OROBOROS_UNITTEST", TRUE);

/**
 * <Custom Data Sources>
 * @wip
 * This provides a flatfile datasource that will run 
 * instead of the database to get the system up long 
 * enough to debug the issue. Running from a flatfile 
 * is extremely slow, and not recommended otherwise. 
 * This will allow a barebones admin login to diagnose 
 * and correct the issue.
 * 
 * <Recover the System if the Database is Down>
 * @wip
 * The default recovery file is located here:
 * /oroboros/data/system/system.recovery.json;
 * 
 * <Rollback Upgrade Installation>
 * @wip
 * You may use the following if you have 
 * recently upgraded and it broke things:
 * /oroboros/data/system/system.rollback.json;
 * 
 * <Reinstall Current Version>
 * @wip
 * You may use the following if you need to 
 * reinstall the current version because it 
 * has been corrupted for some reason:
 * /oroboros/data/system/system.reinstall.json;
 * 
 * <Check for Core Modifications>
 * @wip
 * You may use the following if you need to check the 
 * filebase against the repo for changes, in case you 
 * have been hacked or have modified core files that 
 * have broken your installation. You should only run 
 * this one time, save the report, and disable it. 
 * This is a very server-intensive process and is likely 
 * to timeout if you have a very large, customized 
 * installation. If that is the case, check our wiki 
 * for tips on how to resolve this.
 * /oroboros/data/system/system.validatecore.json;
 */
//define("OROBOROS_DATA_SOURCE", "somefile.json");

/**
 * Let's do this!
 */
require_once realpath(dirname(__FILE__)) . DIRECTORY_SEPARATOR .
        'oroboros' . DIRECTORY_SEPARATOR .
        'src' . DIRECTORY_SEPARATOR .
        'routines' . DIRECTORY_SEPARATOR .
        'frontcontroller.routine.php';
/**
 * And like that, he's gone.
 */
exit();
